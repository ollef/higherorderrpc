#include "runtime/value.h"
Value    TagInteger(intptr_t rawInt) {
  return (rawInt << 1) | LITERAL_BIT_MASK;
}
intptr_t UntagInteger(Value taggedInt) {
  return taggedInt >> 1;
}
bool     IsLiteral(Value val) {
  return (val & LITERAL_BIT_MASK);
}
bool     IsPointer(Value val) {
  return !IsLiteral(val);
}
bool     IsPlainPointer(Value val) {
  return (val & PLAINPTR_BIT_MASK) == 0;
}
bool     IsFunctionPointer(Value val) {
  return (val & PLAINPTR_BIT_MASK) == FUNPTR_BIT_MASK;
}
Value    TagFunctionPointer(Value ptr) {
  return ptr | FUNPTR_BIT_MASK;
}
Value    UntagFunctionPointer(Value val) {
  return val & (~FUNPTR_BIT_MASK);
}
/* bool     IsMobile(Value funid) {
  return UntagInteger(funid) < MobileCutoff;
}
*/
/*
Value    GetRemoteApplyId(Value arity) {
  return TagInteger(UntagInteger(Id_RemoteApply_1) + UntagInteger(arity) - 1);
}
*/
