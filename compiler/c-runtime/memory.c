#include "runtime/memory.h"
#include <gc.h>
#include <stdlib.h>

void InitMemory() {
  GC_INIT();
}

void* Malloc(size_t size) {
  // For now, just reserve a word in each allocation for its size.
  // It's a pity that the Boehm GC doesn't support getting it...
  size_t* block = GC_MALLOC(size + sizeof(size_t));
  // size_t* block = malloc(sizeof(size_t) + size);
  block[0] = size;
  return block + 1;
}

size_t Size(void* ptr) {
  return *(((size_t*) ptr) - 1);
}
