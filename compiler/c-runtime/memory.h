#ifndef MEMORY_H
#define MEMORY_H

#include <stddef.h>

void InitMemory();

// Alloc size bytes
void* Malloc(size_t size);

// Get the size of an allocation
size_t Size(void* bytes);

#endif
