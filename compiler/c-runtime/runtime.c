#include "runtime/runtime.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "mpi.h"
#include "runtime/serial.h"
#include "runtime/value.h"

int MyRank;

#define WARNING(...) fprintf(stderr, "********" __VA_ARGS__);
#define ERROR(...) WARNING(__VA_ARGS__); exit(0);

#ifdef PROFILE
size_t MessagesSent = 0;
size_t BytesSent    = 0;
#endif

void InitRuntime(int argc, char** argv) {
  InitMemory();
  int provided;
  if(MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided) != MPI_SUCCESS) {
    ERROR("MPI_Init failed!\n");
  }
  if(provided != MPI_THREAD_MULTIPLE) {
    WARNING("******* The MPI environment could not provide the required level of\n"
            "******* thread support. This may lead to deadlocks.\n"
            "******* Proceeding...\n");
  }
  MPI_Comm_rank(MPI_COMM_WORLD, &MyRank);
}

#define DCESH_BIN_OP(a, op, b) (TagInteger((UntagInteger(a)) op (UntagInteger(b))))

Value _add (Value a, Value b) { return DCESH_BIN_OP(a, +,  b); }
Value _sub (Value a, Value b) { return DCESH_BIN_OP(a, -,  b); }
Value _mul (Value a, Value b) { return DCESH_BIN_OP(a, *,  b); }
Value _div (Value a, Value b) { return DCESH_BIN_OP(a, /,  b); }
Value _mod (Value a, Value b) { return DCESH_BIN_OP(a, %,  b); }
Value _shl (Value a, Value b) { return DCESH_BIN_OP(a, <<, b); }
Value _shr (Value a, Value b) { return DCESH_BIN_OP(a, >>, b); }
Value _lt  (Value a, Value b) { return DCESH_BIN_OP(a, <,  b); }
Value _gt  (Value a, Value b) { return DCESH_BIN_OP(a, >,  b); }
Value _lteq(Value a, Value b) { return DCESH_BIN_OP(a, <=, b); }
Value _gteq(Value a, Value b) { return DCESH_BIN_OP(a, >=, b); }

#define taggedFalse TagInteger(0)
#define taggedTrue  TagInteger(1)

Value _eq(Value a, Value b) {
  if(IsLiteral(a)) {
    return IsLiteral(b)
      ? TagInteger(a == b)
      : taggedFalse;
  } else if(IsFunctionPointer(a) || IsFunctionPointer(b)) {
    return taggedFalse;
  } else if(IsPlainPointer(a)) {
    if(IsPlainPointer(b)) {
      Value* pa = (Value*) a;
      Value* pb = (Value*) b;
      size_t na = Size(pa) / sizeof(Value);
      size_t nb = Size(pb) / sizeof(Value);
      if(na == nb) {
        for(size_t i = 0; i < na; ++i) {
          if(UntagInteger(_neq(pa[i], pb[i]))) {
            return taggedFalse;
          }
        }
        return taggedTrue;
      } else {
        return taggedFalse;
      }
    } else {
      return taggedFalse;
    }
  }
}
Value _neq(Value a, Value b) {
  return UntagInteger(_eq(a, b)) ? taggedFalse : taggedTrue;
}

#undef taggedFalse
#undef taggedTrue

#undef DCESH_BIN_OP

Value print_int(Value v, Value _) {
  printf("%" PRIdPTR, UntagInteger(v));
  return TagInteger(0);
}

Value print_char(Value v, Value _) {
  putchar((char)UntagInteger(v));
  return TagInteger(0);
}

Value RemoteApplyReceive(Value);

Value HandleApplyMessage(int count, int src) {
  SerialisedValue serialised;
  serialised.length = count / sizeof(Value);
  serialised.values = malloc(count);
  MPI_Recv(serialised.values, count,
           MPI_BYTE, src, APPLY_MSG_TAG,
           MPI_COMM_WORLD, MPI_STATUS_IGNORE);

  Value v = Deserialise(serialised);
  free(serialised.values);
  SerialisedValue serialisedResult = Serialise(RemoteApplyReceive(v));
#ifdef PROFILE
  ++MessagesSent;
  BytesSent += sizeof(Value) * serialisedResult.length;
#endif
  MPI_Send(serialisedResult.values, sizeof(Value) * serialisedResult.length,
           MPI_BYTE, src, RETURN_MSG_TAG, MPI_COMM_WORLD);
}

int Exit = 0;

Value RemoteApplySend(Value node, Value data) {
  SerialisedValue serialised = Serialise(data);
#ifdef PROFILE
  ++MessagesSent;
  BytesSent += sizeof(Value) * serialised.length;
#endif
  MPI_Send(serialised.values, sizeof(Value) * serialised.length, MPI_BYTE, UntagInteger(node), APPLY_MSG_TAG, MPI_COMM_WORLD);
  free(serialised.values);

  while(!Exit) {
    MPI_Status status;
    MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
    int count;
    MPI_Get_count(&status, MPI_BYTE, &count);

    switch (status.MPI_TAG) {
      case APPLY_MSG_TAG: {
        HandleApplyMessage(count, status.MPI_SOURCE);
        break;
      }
      case RETURN_MSG_TAG: {
        SerialisedValue serialised;
        serialised.length = count / sizeof(Value);
        serialised.values = malloc(count);
        MPI_Recv(serialised.values, count,
                 MPI_BYTE, status.MPI_SOURCE, RETURN_MSG_TAG,
                 MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        Value v = Deserialise(serialised);
        free(serialised.values);
        return v;
      }
      case TERMINATE_MSG_TAG: {
        Exit = 1;
        break;
      }
      default:
        ERROR("Expecting return or apply message but got something else (%d)\n",
              status.MPI_TAG);
        break;
    }
  }
}

Value LookupFunction(Value fid) {
  return TagFunctionPointer(FunctionTable[UntagInteger(fid)]);
}

Value IsLocalNode(Value node) {
  return TagInteger(((int) UntagInteger(node)) == MyRank);
}

void MainLoop() {
  while(!Exit) {
    MPI_Status status;
    MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
    int count;
    MPI_Get_count(&status, MPI_BYTE, &count);
    switch (status.MPI_TAG) {
      case APPLY_MSG_TAG: {
        HandleApplyMessage(count, status.MPI_SOURCE);
        break;
      }
      case TERMINATE_MSG_TAG: {
        Exit = 1;
        break;
      }
      default:
        ERROR("Unhandled message");
        break;
    }
  }
#ifdef PROFILE
  printf("Node %d exiting, after having done %zu remote invocations sending a total of %zu bytes\n", MyRank, MessagesSent, BytesSent);
#endif
  MPI_Finalize();
}

Value terminate(Value r) {
  Exit = 1;
  int nodes;
  MPI_Comm_size(MPI_COMM_WORLD, &nodes);
  for(int i = 0; i < nodes; ++i) {
    if(i != MyRank) {
      MPI_Send(0, 0, MPI_BYTE, i, TERMINATE_MSG_TAG, MPI_COMM_WORLD);
    }
  }
  return r;
}
