#ifndef VALUE_H
#define VALUE_H
#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

static const intptr_t LITERAL_BIT_MASK       = 1; // xx1
static const intptr_t FUNPTR_BIT_MASK        = 2; // 010
static const intptr_t PLAINPTR_BIT_MASK      = 7; // 000

typedef intptr_t Value;

Value    TagInteger(intptr_t rawInt);
intptr_t UntagInteger(Value taggedInt);
bool     IsLiteral(Value val);
bool     IsPointer(Value val);
bool     IsPlainPointer(Value val);
bool     IsFunctionPointer(Value val);
Value    TagFunctionPointer(Value ptr);
Value    UntagFunctionPointer(Value val);
// bool     IsMobile(Value funid);
// Value    GetRemoteApplyId(Value arity);

extern const size_t MobileCutoff;
extern const Value  Id_RemoteApply_1;

#endif
