#include "runtime/serial.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include <stdio.h>

#include "runtime/memory.h"

extern int MyRank;

Value internal_pointer(const Value* const start, const Value* const ptr) {
  return (Value)((ptr - start) * sizeof(Value));
}

size_t getNumSlots(const Value val) {
  if(IsPlainPointer(val)) {
    Value* ptr = (Value*) val;
    size_t slots = 2; // Accounts for pointer and size
    size_t n = Size(ptr) / sizeof(Value);
    for(size_t i = 0; i < n; ++i) {
      slots += getNumSlots(ptr[i]);
    }
    return slots;
  } else if(IsFunctionPointer(val)) {
    // NOTE: Overapproximation
    return 3;
  } else {
    return 1;
  }
}

SerialisedValue Serialise(const Value val) {
  const size_t maxlen = getNumSlots(val);
  Value* const values = malloc(sizeof(Value) * maxlen);
  Value* finger = values;
  Value* end = finger + 1;
  *finger = val;
  while(finger < end) {
    if(IsPlainPointer(*finger)) {
      Value* nextFinger = (Value*) (*finger);
      size_t size = Size(nextFinger);
      size_t slots = size / sizeof(Value);
      *end++ = TagInteger(size);
      *finger = internal_pointer(values, end);
      size_t i = 0;
      if(IsFunctionPointer(nextFinger[0])) { // i.e. this is a closure
        const Value funId    = nextFinger[1];
        const Value funArity = nextFinger[2];
        *end++ = TagFunctionPointer(0);
        ++i;
        /*
        if(!IsMobile(funId)) {
          *end++ = GetRemoteApplyId(funArity);
          *end++ = funArity;
          ++i;
          *end++ = funId;
          ++i;
          *end++ = TagInteger(MyRank);
        } 
        */
      }
      for(; i < slots; ++i) {
        *end++ = nextFinger[i];
      }
    }
    ++finger;
  }
  SerialisedValue serialised = { .length = finger - values
                               , .values = values
                               };
  return serialised;
}

Value deserialise_worker(Value* values, Value val) {
  if(IsPlainPointer(val)) {
    Value* const nextPtr = values + val / sizeof(Value);
    size_t size = UntagInteger(*(nextPtr - 1));
    size_t slots = size / sizeof(Value);
    size_t i = 0;
    Value* const res = Malloc(size);
    if(IsFunctionPointer(nextPtr[0])) {
      const Value funId  = nextPtr[1];
      const Value funPtr = LookupFunction(funId);
      res[0] = funPtr;
      ++i;
      res[1] = funId;
      ++i;
    }
    for(; i < slots; ++i) {
      res[i] = deserialise_worker(values, nextPtr[i]);
    }
    return (Value) res;
  } else {
    return val;
  }
}

Value Deserialise(SerialisedValue serialised) {
  return deserialise_worker(serialised.values, *serialised.values);
}
