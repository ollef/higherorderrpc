#ifndef RUNTIME_H
#define RUNTIME_H

#include "runtime/memory.h"
#include "runtime/value.h"

#define APPLY_MSG_TAG 1
#define RETURN_MSG_TAG 3
#define TERMINATE_MSG_TAG 5

void InitRuntime();

Value _add (Value, Value);
Value _sub (Value, Value);
Value _mul (Value, Value);
Value _div (Value, Value);
Value _mod (Value, Value);
Value _shl (Value, Value);
Value _shr (Value, Value);
Value _lt  (Value, Value);
Value _gt  (Value, Value);
Value _lteq(Value, Value);
Value _gteq(Value, Value);
Value _eq  (Value, Value);
Value _neq (Value, Value);

Value print_int (Value, Value);
Value print_char(Value, Value);

Value RemoteApplySend(Value node, Value data);
Value LookupFunction(Value fid);
Value IsLocalNode(Value node);

void MainLoop();

extern const Value FunctionTable[];

Value terminate(Value);

#endif
