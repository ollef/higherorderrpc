#ifndef SERIAL_H
#define SERIAL_H

#include <stdbool.h>
#include <stdlib.h>

#include "runtime/value.h"

typedef struct {
  size_t length;
  Value* values;
} SerialisedValue;

SerialisedValue Serialise(const Value val);
Value           Deserialise(SerialisedValue serialised);

extern Value LookupFunction(Value fid);

#endif
