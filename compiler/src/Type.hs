{-# LANGUAGE DeriveFoldable, DeriveFunctor, DeriveTraversable, ViewPatterns #-}
module Type where

import Bound
import Control.Applicative
import Control.Monad
import Data.Foldable(Foldable(foldMap))
import qualified Data.Set as S
import Data.String
import Data.Traversable
import qualified Data.Vector as V
import Prelude.Extras
import Text.PrettyPrint.ANSI.Leijen(text, punctuate, sep, (<+>))

import qualified Builtin.Names as Builtin
import Class
import Constr
import Util
import Pretty

data Type v
  = Var v
  | Con TCon
  | App (Type v) (Type v)
  | Forall (Scope1 Type v)
  | Wildcard
  deriving (Eq, Foldable, Functor, Traversable, Show)

type TypeC  = Type TCon
type TypeCV = Type (Either TCon TVar)

instance Eq1 Type
instance Show1 Type

instance Monad Type where
  return = Var
  typ >>= f = case typ of
    Var v    -> f v
    Con c    -> Con c
    App t t' -> App (t >>= f) (t' >>= f)
    Forall s -> Forall (s >>>= f)
    Wildcard -> Wildcard

instance Applicative Type where
  pure  = return
  (<*>) = ap

instance HasApp (Type v) where
  app = App
  appView (App t t') = pure (t, t')
  appView _          = empty

instance HasAbs Type where
  abs1 x t = Forall $ abstract1 x t
  absView (Forall x) = pure x
  absView _          = empty

instance HasArrow (Type v) where
  t --> t'  = apps (Con Builtin.arrow) [t, t']
  arrowView (appsView -> (Con c, [t, t'])) | c == Builtin.arrow = pure (t, t')
  arrowView _ = empty

quantify :: TypeCV -> TypeC
quantify t = fmap (either id $ error "quantify") $ abss fv t
  where fv = filter isRight $ S.toList $ foldMap S.singleton t
        isRight (Right _) = True
        isRight _         = False

tupleType :: [Type v] -> Type v
tupleType types = Con tupleCon `apps` types
  where
    tupleCon = Builtin.tupleT n
    n = length types

listType :: Type v -> Type v
listType t = Con Builtin.listT `App` t

--------------------------------------------------------------------------------
-- * Type and data definitions
data TypeDef v = TypeDef Int (Scope Int Type v)
  deriving (Eq, Foldable, Functor, Traversable, Show)
data DataDef v = DataDef Int [(ECon, [Scope Int Type v])]
  deriving (Eq, Foldable, Functor, Traversable, Show)

type TypeDefC = TypeDef TCon
type DataDefC = DataDef TCon

typeDef :: [TVar] -> TypeCV -> TypeDefC
typeDef vs t = TypeDef (length vs) $ either id err <$> abstract abstr t
  where fv = V.fromList vs
        abstr = either (const Nothing) (`V.elemIndex` fv)
        err v = error $ "typeDef, not in scope: " ++ show v

dataDef :: [TVar] -> [(ECon, [TypeCV])] -> DataDefC
dataDef vs cs =
  DataDef (length vs)
          [(c, [either id err <$> abstract abstr t | t <- ts])
          | (c, ts) <- cs]
  where fv = V.fromList vs
        abstr = either (const Nothing) (`V.elemIndex` fv)
        err v = error $ "dataDef, not in scope: " ++ show v

--------------------------------------------------------------------------------
-- Pretty-printing
instance (IsString v, Pretty v) => Pretty (Type v) where
  prettyPrec ns d typ = case typ of
    (arrowView -> Just (t1, t2)) -> parensIf (d > arrPrec) $
      prettyPrec ns (arrPrec + 1) t1 <+> text "->" <+> prettyPrec ns arrPrec t2
    Var v    -> prettyPrec ns d v
    Con c    -> prettyPrec ns d c
    App t t' -> prettyApp ns d t t'
    Forall s -> prettyAbs ns d "forall" "." (\v -> instantiate1 (return v) s)
    Wildcard -> text "_"

instance (IsString v, Pretty v) => Pretty (TypeDef v) where
  prettyPrec ns d (TypeDef n s) = prettyAbss ns d "type" "=" n $ \ns' d' f ->
    prettyPrec ns' d' $ instantiate (return . f) s

instance (IsString v, Pretty v) => Pretty (DataDef v) where
  prettyPrec ns d (DataDef n cs) = prettyAbss ns d "data" "=" n $ \ns' _ f ->
    let inst = instantiate (return . f) in
      sep $ punctuate (text " | ")
              [prettyApps ns' 0 (prettyPrecF c) (map (prettyPrecF . inst) ss)
              | (c, ss) <- cs]
