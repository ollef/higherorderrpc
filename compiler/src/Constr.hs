-- | Constructors and variables (!)
module Constr where

import Data.String

import Pretty
import qualified Text.PrettyPrint.ANSI.Leijen as PP

newtype EVar  = EVar String deriving (Eq, Ord, Show)
newtype ECon  = ECon String deriving (Eq, Ord, Show)
newtype Node  = Node {unNode :: String} deriving (Eq, Ord, Show)
newtype TVar  = TVar String deriving (Eq, Ord, Show)
newtype TCon  = TCon String deriving (Eq, Ord, Show)

instance Pretty EVar where pretty _ (EVar s) = PP.string s
instance Pretty ECon where pretty _ (ECon s) = PP.string s
instance Pretty Node where pretty _ (Node s) = PP.string s
instance Pretty TVar where pretty _ (TVar s) = PP.string s
instance Pretty TCon where pretty _ (TCon s) = PP.string s

instance IsString EVar where fromString = EVar
instance IsString ECon where fromString = ECon
instance IsString Node where fromString = Node
instance IsString TVar where fromString = TVar
instance IsString TCon where fromString = TCon

data Literal
  = IntL Int
  | CharL Char
  deriving (Eq, Ord, Show)

instance Pretty Literal where
  pretty _ (IntL i)  = PP.pretty i
  pretty _ (CharL c) = PP.pretty $ show c
