{-# LANGUAGE DeriveFoldable, DeriveFunctor, DeriveTraversable, FlexibleContexts #-}
-- | Case branches
module Branches where

import Control.Applicative
import Data.Bitraversable
import Data.Foldable
import Data.String
import Data.Traversable
import Text.PrettyPrint.ANSI.Leijen(align, text, vcat, (<+>))

import Bound
import Bound.Scope
import Constr
import Pretty

data Branches b v
  = ConBranches [ConBranch b v]       -- ^ Must be total
  | LitBranches [LitBranch b v] (b v) -- ^ Has default branch
  deriving (Eq, Ord, Show, Functor, Foldable, Traversable)

data ConBranch b v = ConBranch ECon Int (Scope Int b v)
  deriving (Eq, Ord, Show, Functor, Foldable, Traversable)
data LitBranch b v = LitBranch Literal (b v)
  deriving (Eq, Ord, Show, Functor, Foldable, Traversable)

mapBranches :: Functor b => (b v -> b v') -> Branches b v -> Branches b v'
mapBranches f (ConBranches brs) =
  ConBranches [ConBranch c i $ Scope $ fmap f <$> s | ConBranch c i (Scope s) <- brs]
mapBranches f (LitBranches brs d) =
  LitBranches [LitBranch n (f b) | LitBranch n b <- brs] (f d)

instance Bound Branches where
  ConBranches cbrs >>>= f = ConBranches (map (>>>= f) cbrs)
  LitBranches lbrs d >>>= f = LitBranches (map (>>>= f) lbrs) (d >>= f)

instance Bound ConBranch where
  ConBranch c i s >>>= f = ConBranch c i (s >>>= f)
instance Bound LitBranch where
  LitBranch n b   >>>= f = LitBranch n (b >>= f)

bitraverseConBranch :: (Bitraversable t, Applicative f)
                    => (v -> f v') -> (a -> f a') -> ConBranch (t v) a -> f (ConBranch (t v') a')
bitraverseConBranch f g (ConBranch c n s) = ConBranch c n <$> bitraverseScope f g s
bitraverseLitBranch :: (Bitraversable t, Applicative f)
                    => (v -> f v') -> (a -> f a') -> LitBranch (t v) a -> f (LitBranch (t v') a')
bitraverseLitBranch f g (LitBranch l b) = LitBranch l <$> bitraverse f g b

bitraverseBranches :: (Bitraversable t, Applicative f)
                 => (v -> f v') -> (a -> f a')
                 -> Branches (t v) a -> f (Branches (t v') a')
bitraverseBranches f g (ConBranches brs) = ConBranches <$> traverse (bitraverseConBranch f g) brs
bitraverseBranches f g (LitBranches brs e) = LitBranches <$> traverse (bitraverseLitBranch f g) brs
                                                         <*> bitraverse f g e

instance (Monad e, Pretty (e v), Pretty v, IsString v) => Pretty (Branches e v) where
  prettyPrec ns d (ConBranches cbrs)     = vcat $ map (prettyPrec ns d) cbrs
  prettyPrec ns d (LitBranches lbrs def) =
       vcat $ map (prettyPrec ns d) lbrs
    ++ [text "_" <+> text "->" <+> prettyPrec ns 0 def]

instance (Monad e, Pretty (e v), Pretty v, IsString v) => Pretty (ConBranch e v) where
  prettyPrec ns d (ConBranch c numVars s) = prettyGenVars ns d numVars $ \fvs _ bvs lu ->
    prettyApps fvs 0 (prettyPrecF c) (map (\bv _ _ -> text bv) bvs) <+>
    text "->" <+> align (pretty fvs $ instantiate (return . lu) s)

instance (Pretty (e v), Pretty v, IsString v) => Pretty (LitBranch e v) where
  prettyPrec ns _ (LitBranch l e) =
    pretty ns l <+> text "->" <+> align (pretty ns e)
