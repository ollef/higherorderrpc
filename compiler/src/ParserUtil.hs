module ParserUtil where

import Control.Monad.State
import Data.Char (digitToInt)
import Data.Text (Text)
import Text.Parsec hiding (State)
import Text.Parsec.Expr
import Text.Parsec.Text()

type Parser  a = ParsecT       Text () (State SourcePos) a
type OpTable a = OperatorTable Text () (State SourcePos) a

-- Character and string literal parsing adapted from Text.Parsec.Token

charLiteral :: Parser Char
charLiteral = between (char '\'')
                      (char '\'' <?> "end of character")
                      characterChar
           <?> "character"

characterChar :: Parser Char
characterChar = charLetter <|> charEscape
             <?> "literal character"

charEscape :: Parser Char
charEscape = char '\\' >> escapeCode

charLetter :: Parser Char
charLetter = satisfy (\c -> (c /= '\'') && (c /= '\\') && (c > '\026'))

stringLiteral :: Parser String
stringLiteral = liftM (foldr (maybe id (:)) "")
                      (between (char '"')
                               (char '"' <?> "end of string")
                               (many stringChar))
             <?> "literal string"

stringChar :: Parser (Maybe Char)
stringChar =  liftM Just stringLetter
          <|> stringEscape
          <?> "string character"

stringLetter :: Parser Char
stringLetter = satisfy (\c -> (c /= '"') && (c /= '\\') && (c > '\026'))

stringEscape :: Parser (Maybe Char)
stringEscape = char '\\' >> (liftM (const Nothing) escapeGap
                         <|> liftM (const Nothing) escapeEmpty
                         <|> liftM Just escapeCode)

escapeEmpty :: Parser Char
escapeEmpty = char '&'

escapeGap :: Parser Char
escapeGap = many1 space >> (char '\\' <?> "end of string gap")

-- escape codes
escapeCode :: Parser Char
escapeCode = charEsc <|> charNum <|> charAscii <|> charControl
          <?> "escape code"

charControl :: Enum a => Parser a
charControl = char '^' >> upper >>= (\code -> return . toEnum $ fromEnum code - fromEnum 'A')

charNum :: Enum a => Parser a
charNum = liftM (toEnum . fromInteger)
                (number 10 digit
                <|> (char 'o' >> number 8 octDigit)
                <|> (char 'x' >> number 16 hexDigit))

charEsc :: Parser Char
charEsc = choice $ map parseEsc escMap
  where
    parseEsc :: (Char, Char) -> Parser Char
    parseEsc (c,code) = char c >> return code

charAscii :: Parser Char
charAscii = choice $ map parseAscii asciiMap
  where
    parseAscii :: (String, Char) -> Parser Char
    parseAscii (asc,code) = try $ string asc >> return code

-- escape code tables
escMap :: [(Char, Char)]
escMap = zip "abfnrtv\\\"\'" "\a\b\f\n\r\t\v\\\"\'"

asciiMap :: [(String, Char)]
asciiMap = zip (ascii3codes ++ ascii2codes) (ascii3 ++ ascii2)

ascii2codes, ascii3codes :: [String]
ascii2codes = ["BS","HT","LF","VT","FF","CR","SO",
               "SI","EM","FS","GS","RS","US","SP"]
ascii3codes = ["NUL","SOH","STX","ETX","EOT",
               "ENQ","ACK","BEL","DLE","DC1",
               "DC2","DC3","DC4","NAK","SYN",
               "ETB","CAN","SUB","ESC","DEL"]

ascii2, ascii3 :: [Char]
ascii2 = ['\BS','\HT','\LF','\VT','\FF','\CR','\SO',
          '\SI','\EM','\FS','\GS','\RS','\US','\SP']
ascii3 = ['\NUL','\SOH','\STX','\ETX','\EOT',
          '\ENQ','\ACK','\BEL','\DLE','\DC1',
          '\DC2','\DC3','\DC4','\NAK','\SYN',
          '\ETB','\CAN','\SUB','\ESC','\DEL']

number :: Integer -> Parser Char -> Parser Integer
number base baseDigit = do
  digits <- many1 baseDigit
  let n = foldl (\x d -> base*x + toInteger (digitToInt d)) 0 digits
  seq n $ return n
