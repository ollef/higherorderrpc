module Parser where

import Control.Applicative((<$>), (<$), (<*>), (<*), (*>))
import Control.Monad.State
import Data.Monoid
import Data.Ord
import Data.Set(Set)
import qualified Data.Set as S
import Data.Text(Text)
import Data.Text.IO as TextIO
import qualified Data.Text as Text
import Data.Vector(Vector)
import qualified Data.Vector as V
import Text.Parsec hiding (State)
import Text.Parsec.Expr
import Text.Parsec.Pos
import Text.Parsec.Text()

import qualified Builtin.Functions as Builtin
import qualified Builtin.Types as Builtin
import Class
import Constr
import Input.Expr as Expr
import Type
import Input.Prog
import ParserUtil hiding (number)

{-
import Debug.Trace

dbg :: Show a => String -> Parser a -> Parser a
dbg s p = do
  st <- get
  pos <- getPosition
  res <- trace ("trying " ++ s ++ " " ++ show pos ++ " st: " ++ show st) p
  pos' <- getPosition
  trace (s ++ " " ++ show pos' ++ " = " ++ show res) $ return res
-}

dbg :: String -> Parser a -> Parser a
dbg = const id

parseText :: Parser a -> Text -> String -> Either ParseError a
parseText p t s = evalState (runParserT (whitespace *> p <* eof) () s t) (initialPos s)

parseString :: Parser a -> String -> Either ParseError a
parseString p s = parseText p (Text.pack s) "test"

parseFile :: Parser a -> FilePath -> IO (Either ParseError a)
parseFile p fp = do
  t <- TextIO.readFile fp
  return $ parseText p t fp

pass :: FilePath -> IO Prog
pass fp = do
  res <- parseFile program fp
  return $ case res of
    Left err -> error $ show err
    Right p  -> p `mappend` mconcat (map (uncurry dataProg) Builtin.dataDefinitions)

-- | Drop the indentation anchor -- use the current position as the reference
--   point in the given parser.
dropAnchor :: Parser a -> Parser a
dropAnchor p = do
  oldAnchor <- get
  pos       <- getPosition
  put pos
  result    <- p
  put oldAnchor
  return result

-- | Check that the current indentation level is the same as the anchor
sameCol :: Parser ()
sameCol = do
  pos    <- getPosition
  anchor <- get
  case comparing sourceColumn pos anchor of
    LT -> unexpected "unindent"
    EQ -> return ()
    GT -> unexpected "indent"

-- | Check that the current indentation level is on the same line as the anchor
--   or on a successive line but more indented.
sameLineOrIndented :: Parser ()
sameLineOrIndented = do
  pos    <- getPosition
  anchor <- get
  case (comparing sourceLine pos anchor, comparing sourceColumn pos anchor) of
    (EQ, _)  -> return () -- Same line
    (GT, GT) -> return () -- Indented
    (_,  _)  -> unexpected "unindent"

-- | One or more at the same indentation level.
many1SameCol :: Parser a -> Parser [a]
many1SameCol p = many1 $ sameCol >> p

-- | Zero or more at the same indentation level.
manySameCol :: Parser a -> Parser [a]
manySameCol p = many $ sameCol >> p

-- | One or more on the same line or a successive but indented line.
many1SI :: Parser a -> Parser [a]
many1SI p = many1 $ sameLineOrIndented >> p
--
-- | Zero or more on the same line or a successive but indented line.
manySI :: Parser a -> Parser [a]
manySI p = many $ sameLineOrIndented >> p

-- | Zero or more, separated by commas, on the same line or a successive but indented line.
commaSepSI :: Parser a -> Parser [a]
commaSepSI p = sepBy (sameLineOrIndented >> p) (sameLineOrIndented >> r",")

-- * Applicative style combinators for checking that the second argument parser
--   is on the same line or indented compared to the anchor.
infixl 4 <$>%, <$%, <*>%, <*%, *>%
(<$>%) :: (a -> b) -> Parser a -> Parser b
f <$>% p = f <$> (sameLineOrIndented >> p)
(<$%) :: a -> Parser b -> Parser a
f <$%  p = f <$  (sameLineOrIndented >> p)
(<*>%) :: Parser (a -> b) -> Parser a -> Parser b
p <*>% q = p <*> (sameLineOrIndented >> q)
(<*%) :: Parser a -> Parser b -> Parser a
p <*%  q = p <*  (sameLineOrIndented >> q)
(*>%) :: Parser a -> Parser b -> Parser b
p *>%  q = p *>  (sameLineOrIndented >> q)

whitespace :: Parser ()
whitespace = skipMany (void space <|> void comment) <?> "whitespace"
  where
    comment =  (try (string "--") >> manyTill anyChar (void newline))
           <|> (try (string "{-") >> manyTill anyChar (try $ string "-}"))

tok :: Parser a -> Parser a
tok p = try p <* whitespace

r :: String -> Parser String
r = tok . string

reserved :: Set String
reserved = S.fromList [ "let", "in", "where"
                      , "case", "of"
                      , "data", "type", "forall"
                      , "if", "then", "else"
                      ]

startVarLetter :: Parser Char
startVarLetter = lower

startConLetter :: Parser Char
startConLetter = upper

startNodeLetter :: Parser Char
startNodeLetter = upper

identLetter :: Parser Char
identLetter = alphaNum <|> char '_' <|> char '\''

number :: Parser Int
number = read <$> tok (many1 digit)
      <?> "number"

var :: Parser String
var = tok (do
  s <- (:) <$> startVarLetter <*> many identLetter
  if s `S.member` reserved
  then unexpected $ "reserved identifier: " ++ show s
  else return s) <?> "identifier"

evar :: Parser EVar
evar = EVar <$> var

tvar :: Parser TVar
tvar = TVar <$> var

con :: Parser String
con = tok ((:) <$> startConLetter <*> many identLetter)
   <?> "constructor"

econ :: Parser ECon
econ = ECon <$> con

tcon :: Parser TCon
tcon = TCon <$> con

node :: Parser String
node = tok ((:) <$> startNodeLetter <*> many identLetter)
    <?> "node"

enode :: Parser Node
enode = Node <$> node

afterwards :: Parser a -> Parser (a -> a) -> Parser a
afterwards p after = do
  result <- p
  f      <- option id (id <$>% after)
  return $ f result

defClause :: Parser ([PatV], ExprV)
defClause = dbg "defClause" (
  dropAnchor ((,) <$> manySI atomicPat <*% r"=" <*>% expr))
  <?> "clause"

defClauses :: Parser [([PatV], ExprV)]
defClauses = dbg "defClauses" (
  dropAnchor (many1SameCol defClause) <?> "clauses")

def :: Parser (EVar, Maybe Node, Maybe TypeC, SmartClauses TypeC EVar)
def = (,,,) <$> evar <*>% optionalAtNode <*>% optionalTypeSig <*> defClauses
   <?> "definition"
  where
    optionalAtNode  = optionMaybe (dropAnchor $ r"@" *>% enode)
    optionalTypeSig = optionMaybe (dropAnchor $ r":" *>% (quantify <$> typp))

defs :: Parser (Vector (EVar, Maybe Node, Maybe TypeC, SmartClauses TypeC EVar))
defs = dropAnchor $ V.fromList <$> many1SameCol (dropAnchor def)

expr :: Parser ExprV
expr = buildExpressionParser opTable
       (abss <$ r"\\" <*> many1SI evar <*% r"." <*>% expr
    <|> lett <$ r"let" <*>% defs <*% r"in" <*>% expr
    <|> iff  <$ r"if" <*>% expr <*% r"then" <*>% expr <*% r"else" <*>% expr
    <|> casee <$ r"case" <*>% expr <*% r"of" <*>% branches
    <|> apps <$> atomicExpr <*> manySI atomicExpr
    ) `afterwards`
    (lett <$ r"where" <*>% defs)
      `afterwards`
    (flip AtNode <$ r"@" <*>% enode)
      `afterwards`
    (flip Anno <$ r":" <*>% (quantify <$> typp))
    <?> "expression"

atomicExpr :: Parser ExprV
atomicExpr =  Expr.Var <$> evar
          <|> Lit . IntL  <$> number
          <|> Lit . CharL <$>% tok charLiteral
          <|> Expr.Con <$> econ
          <|> strLit <$> tok stringLiteral
          <|> listLit <$ r"[" <*>% commaSepSI expr <*% r"]"
          <|> try (r"(" *>% expr <*% r")")
          <|> tupleLit <$ r"(" <*>% commaSepSI expr <*% r")"
          <?> "atomic expression"

opTable :: OpTable ExprV
opTable =
  [ [binOp "."  Builtin.compose AssocRight]
  , [binOp "*"  Builtin.mul     AssocLeft,
     binOp "/"  Builtin.div     AssocLeft,
     binOp "%"  Builtin.mod     AssocLeft]
  , [binOp "+"  Builtin.add     AssocLeft,
     binOp "-"  Builtin.sub     AssocLeft]
  , [binOp "<<" Builtin.shl     AssocLeft,
     binOp ">>" Builtin.shr     AssocLeft]
  , [binOp "::" Builtin.cons    AssocRight,
     binOp "++" Builtin.append  AssocRight]
  , [binOp "==" Builtin.eq      AssocNone,
     binOp "/=" Builtin.neq     AssocNone,
     binOp "<=" Builtin.lteq    AssocNone,
     binOp ">=" Builtin.gteq    AssocNone,
     binOp "<"  Builtin.lt      AssocNone,
     binOp ">"  Builtin.gt      AssocNone]
  , [binOp "&&" Builtin.and     AssocRight]
  , [binOp "||" Builtin.or      AssocRight]
  , [binOp ";"  Builtin.seq     AssocRight]
  ]
  where
    binOp sym fun = Infix (p sym >> whitespace >> return fun)
    p sym = try (r sym >> notFollowedBy (char '+' <|> char '=') >> whitespace)

branches :: Parser [(PatV, ExprV)]
branches = dropAnchor (many1SameCol ((,) <$> pat <*% r"->" <*>% expr))

pat :: Parser PatV
pat =  (ConP <$> econ <*> manySI atomicPat
   <|> atomicPat
   ) `afterwards`
   (flip consPat <$ r"::" <*>% pat)
   <?> "pattern"

atomicPat :: Parser PatV
atomicPat = dbg "atomicPat" (
  flip ConP [] <$> econ
  <|> VarP <$> evar
  <|> WildP <$ r"_"
  <|> LitP . IntL  <$> number
  <|> LitP . CharL <$>% tok charLiteral
  <|> strPat <$> tok stringLiteral
  <|> listPat <$ r"[" <*>% commaSepSI pat <*% r"]"
  <|> try (r"(" *>% pat <*% r")")
  <|> tuplePat <$ r"(" <*>% commaSepSI pat <*% r")"
  <?> "atomic pattern")

typp :: Parser TypeCV
typp =  (abss <$% r"forall" <*> many1SI (Right <$> tvar) <*% r"." <*>% typp
    <|> apps <$> atomicType <*> manySI atomicType)
      `afterwards` (flip (-->) <$ r"->" <*>% typp)
    <?> "type"

-- at this point we parse type constructors as variables for easy access
atomicType :: Parser TypeCV
atomicType =  (Type.Var . Left)  <$> tcon
          <|> (Type.Var . Right) <$> tvar
          <|> Type.Wildcard  <$ r"_"
          <|> try (r"(" *>% typp <*% r")")
          <|> tupleType <$ r"(" <*>% commaSepSI typp <*% r")"
          <|> listType <$ r"[" <*>% typp <*% r"]"
          <?> "atomic type"

typeDefinition :: Parser Prog
typeDefinition = typeProg <$ r"type" <*>% tcon <*> (typeDef <$> manySI tvar <*% r"=" <*>% typp)
              <?> "type definition"

dataDefinition :: Parser Prog
dataDefinition = dbg "dataDefinition" (
  dataProg <$ r"data" <*>% tcon <*> (dataDef <$> manySI tvar <*% r"=" <*>% constrDefs)
           <?> "data definition")

constrDef :: Parser (ECon, [TypeCV])
constrDef = dbg "constrDef" (
  (,) <$> econ <*> manySI atomicType
      <?> "constructor definition")

constrDefs :: Parser [(ECon, [TypeCV])]
constrDefs = dbg "constrDefs" (
  (:) <$> constrDef <*> manySI (r"|" *>% constrDef)
      <?> "constructor definitions")

topLevelDef :: Parser Prog
topLevelDef = dbg "topLevelDef" $
  dropAnchor ((\(n, mn, mt, cls) -> defProg n mn mt cls) <$> def
             <|> typeDefinition
             <|> dataDefinition
             <?> "top-level definition")

program :: Parser Prog
program = foldr mappend mempty <$> dropAnchor (manySameCol topLevelDef)
