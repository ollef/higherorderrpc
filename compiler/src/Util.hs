module Util where

import Bound
import Data.Foldable(fold)
import Data.Map(Map, mapWithKey)
import Data.Monoid(Monoid)
import Pretty

type Scope1   = Scope ()
type LetScope = Scope Int

instantiateSome :: Functor f => (b -> Var b' (f a)) -> Scope b f a -> Scope b' f a
instantiateSome f (Scope s) = Scope $ fmap g s
  where
    g (B b)  = f b
    g (F fa) = F fa

abstractNone :: Monad f => f a -> Scope b f a
abstractNone = abstract (const Nothing)

data Empty
instance Eq Empty where (==) = error "Empty"
instance Ord Empty where compare = error "Empty"
instance Show Empty where show = error "Empty"
instance Pretty Empty where pretty = error "Empty"

foldMapWithKey :: Monoid m => (k -> a -> m) -> Map k a -> m
foldMapWithKey f = fold . mapWithKey f
