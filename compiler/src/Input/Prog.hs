{-# LANGUAGE DeriveFoldable, DeriveFunctor, DeriveTraversable #-}
module Input.Prog where

import Bound
import Control.Applicative
import Data.Foldable(Foldable(foldMap))
import Data.Map(Map)
import qualified Data.Map as M
import Data.Maybe
import Data.Monoid
import Data.String
import Data.Traversable
import qualified Data.Vector as V
import Text.PrettyPrint.ANSI.Leijen(align, indent, text, vcat, hsep, (<+>), (<$$>))

import Constr
import Input.Expr as Expr
import Pretty
import Type

data Def t v = Def (Maybe Node) (Maybe t) [([Pat Int], Scope Int (Expr t) v)]
  deriving (Eq, Foldable, Functor, Traversable, Show)

type DefV = Def TypeC EVar

data Prog = Prog
  { definitions     :: Map EVar DefV
  , typeDefinitions :: Map TCon (Either TypeDefC DataDefC)
  , constructors    :: Map ECon (TCon, DataDefC)
  } deriving (Show)

instance Monoid Prog where
  mempty = Prog mempty mempty mempty
  mappend (Prog d1 t1 c1) (Prog d2 t2 c2)
    = Prog (M.unionWithKey errD d1 d2)
           (M.unionWithKey errT t1 t2)
           (M.unionWithKey errC c1 c2)
      where
        errD n _ _ = error $ "duplicate definition: " ++ show n
        errT t _ _ = error $ "duplicate type constructor definition: " ++ show t
        errC c _ _ = error $ "duplicate constructor name: " ++ show c

-------------------------------------------------------------------------------
-- * Smart constructors
defProg :: EVar -> Maybe Node -> Maybe TypeC -> SmartClausesCV -> Prog
defProg n mn mt cls = mempty { definitions = M.singleton n $ Def mn mt $ do
    (ps, e) <- cls
    let patix = (`V.elemIndex` foldMap (foldMap V.singleton) ps)
    return (fmap (fromJust . patix) <$> ps, abstract patix e)
  }

typeProg :: TCon -> TypeDefC -> Prog
typeProg n td = mempty { typeDefinitions = M.singleton n (Left td) }

-- TODO check that constructor names are unique
dataProg :: TCon -> DataDefC -> Prog
dataProg name dd@(DataDef _ cs) = mempty
  { typeDefinitions = M.singleton name (Right dd)
  , constructors    = M.fromList [(c, (name, dd)) | (c, _) <- cs]
  }

-------------------------------------------------------------------------------
-- Pretty printing
instance Pretty Prog where
  pretty ns (Prog defs _ _) = vcat -- TODO print types and datas too?
    [pretty ns n <+> prettyFree d | (n, d) <- M.toList defs]

instance (Pretty t, IsString v, Pretty v) => Pretty (Def t v) where
  pretty ns (Def mn mt cls) = case (mn, mt) of -- text "@" <+> align (pretty ns n
    (Just n,  Just t)  -> pn n <$$> pt t <$$> indent 2 pcls
    (Just n,  Nothing) -> pn n <$$> indent 2 pcls
    (Nothing, Just t)  -> pt t <$$> indent 2 pcls
    (Nothing, Nothing) -> align pcls
    where
      pn n = text "@" <+> align (pretty ns n)
      pt t = text ":" <+> align (pretty ns t)
      pcls = vcat $ map prettyClause cls
      prettyClause (ps, s) = prettyGenVars ns 0 numVars $ \ns' _ _ pat ->
        hsep (map (prettyPrec ns' (appPrec + 1) . fmap pat) ps) <+>
        text "=" <+>
        align (prettyPrec ns' 0 $ instantiate (return . pat) s)
          where
            numVars = length $ foldMap (foldMap (:[])) ps
