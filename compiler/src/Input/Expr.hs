{-# LANGUAGE DeriveFoldable, DeriveFunctor, DeriveTraversable, FlexibleContexts #-}
module Input.Expr where

import Bound
import Bound.Scope
import Control.Applicative
import Control.Monad
import Data.Bifoldable
import Data.Bifunctor
import Data.Bitraversable
import Data.Foldable(Foldable(foldMap))
import Data.Maybe
import Data.Monoid
import Data.String
import Data.Traversable
import Data.Vector(Vector)
import qualified Data.Vector as V
import Prelude.Extras
import Text.PrettyPrint.ANSI.Leijen(align, indent, text, vcat, hsep, (<+>), (<$$>))

import qualified Builtin.Names as Builtin
import Class
import Constr
import Pretty
import Type(TypeC)
import Util

-------------------------------------------------------------------------------
-- | Expressions with types of type @t@ and free variables of type @v@
data Expr t v
  = Var v
  | Con ECon
  | Lit Literal
  | Let (Vector (Maybe Node, Maybe t, Clauses t v)) (LetScope (Expr t) v)
  | Lam (Scope1 (Expr t) v)
  | App (Expr t v) (Expr t v)
  | Case (Expr t v) [Branch (Expr t) v]
  | Anno (Expr t v) t
  | AtNode (Expr t v) Node
  deriving (Eq, Foldable, Functor, Traversable, Show)

type ExprV = Expr TypeC EVar
                                        -- Left for patterns, Right for defs
type Clauses t v = [([Pat Int], Scope (Either Int Int) (Expr t) v)]

instance Eq t => Eq1 (Expr t)
instance Show t => Show1 (Expr t)

bindExpr :: (t -> t') -> (v -> Expr t' v') -> Expr t v -> Expr t' v'
bindExpr f g expr = case expr of
  Var v       -> g v
  Con c       -> Con c
  Lit l       -> Lit l
  Let xs e    -> Let (bimap (fmap f) (map (second $ bindExprScope f g)) <$> xs)
                     (bindExprScope f g e)
  Lam s       -> Lam $ bindExprScope f g s
  App e e'    -> App (bindExpr f g e) (bindExpr f g e')
  Case e as   -> Case (bindExpr f g e) (bindBranch f g <$> as)
  Anno e t    -> Anno (bindExpr f g e) (f t)
  AtNode e n  -> AtNode (bindExpr f g e) n

bindExprScope :: (t -> t') -> (v -> Expr t' v')
              -> Scope b (Expr t) v -> Scope b (Expr t') v'
bindExprScope f g (Scope s) = Scope $ bindExpr f (return . fmap (bindExpr f g)) s

instance Monad (Expr t) where
  return = Var
  e >>= f = bindExpr id f e

instance Applicative (Expr t) where
  pure  = return
  (<*>) = ap

instance Bifunctor Expr where
  bimap = bimapDefault
instance Bifoldable Expr where
  bifoldMap = bifoldMapDefault
instance Bitraversable Expr where
  bitraverse f g expr = case expr of
    Var v       -> Var <$> g v
    Con c       -> pure $ Con c
    Lit l       -> pure $ Lit l
    Let xs e    -> Let <$> traverse (bitraverse (traverse f) (traverse (bitraverse pure (bitraverseScope f g)))) xs
                       <*> bitraverseScope f g e
    Lam s       -> Lam <$> bitraverseScope f g s
    App e e'    -> App <$> bitraverse f g e <*> bitraverse f g e'
    Case e as   -> Case <$> bitraverse f g e <*> traverse (bitraverseBranch f g) as
    Anno e t    -> Anno <$> bitraverse f g e <*> f t
    AtNode e n  -> AtNode <$> bitraverse f g e <*> pure n

instance HasApp (Expr t v) where
  app = App
  appView (App e e') = pure (e, e')
  appView _          = empty

instance HasAbs (Expr t) where
  abs1 x e = Lam $ abstract1 x e
  absView (Lam s) = pure s
  absView _       = empty

-------------------------------------------------------------------------------
-- | Patterns
data Pat v
  = VarP v | WildP
  | LitP Literal
  | ConP ECon [Pat v]
  deriving (Eq, Foldable, Functor, Traversable, Show)

type PatV = Pat EVar

instance Eq1 Pat
instance Show1 Pat

-------------------------------------------------------------------------------
-- | Branches
data Branch e v = Branch (Pat Int) (Scope Int e v)
  deriving (Eq, Foldable, Functor, Traversable, Show)

bitraverseBranch :: (Bitraversable t, Applicative f)
                 => (v -> f v') -> (a -> f a')
                 -> Branch (t v) a -> f (Branch (t v') a')
bitraverseBranch f g (Branch ps e) = Branch ps <$> bitraverseScope f g e

instance Bound Branch where
  Branch ps e >>>= f = Branch ps $ e >>>= f

bindBranch :: (t -> t') -> (v -> Expr t' v')
           -> Branch (Expr t) v -> Branch (Expr t') v'
bindBranch f g (Branch ps e) = Branch ps (bindExprScope f g e)

-------------------------------------------------------------------------------
-- * Smart constructors
type SmartClauses t v = [([Pat v], Expr t v)]
type SmartClausesCV = SmartClauses TypeC EVar

lett :: Eq v => Vector (v, Maybe Node, Maybe t, SmartClauses t v) -> Expr t v -> Expr t v
lett bs expr = Let (fmap go bs) $ abstrBody expr
  where
    name (v, _, _, _) = v
    vs                = name <$> bs
    abstrBody         = abstract (`V.elemIndex` vs)
    abstrCl patix     = abstract f
      where
        f x = fmap Left (patix x) <|> fmap Right (x `V.elemIndex` vs)
    go (_, mn, mt, ps)  = (mn, mt, fmap go' ps)
    go' (ps, e) = (fmap (fromJust . patix) <$> ps, abstrCl patix e)
      where patix = (`V.elemIndex` foldMap (foldMap V.singleton) ps)

casee :: Eq v => Expr t v -> [(Pat v, Expr t v)] -> Expr t v
casee expr brs = Case expr $ map go brs
  where
    go (p, e) = Branch (fmap (fromJust . ix) p)
                       (abstract ix e)
      where
        ix = (`V.elemIndex` foldMap V.singleton p)

iff :: Eq v => Expr t v -> Expr t v -> Expr t v -> Expr t v
iff e t f = casee e
  [Builtin.true  #-> t
  ,Builtin.false #-> f
  ]
  where x #-> y = (ConP x [], y)

listLit :: [Expr t v] -> Expr t v
listLit = foldr (\h -> App (App (Con Builtin.cons) h)) (Con Builtin.nil)

consLit :: Expr t v -> Expr t v -> Expr t v
consLit h = App (App (Con Builtin.cons) h)

strLit :: String -> Expr t v
strLit = listLit . map (Lit . CharL)

tupleLit :: [Expr t v] -> Expr t v
tupleLit exprs = tupleLit' (length exprs) $ reverse exprs
  where
  tupleLit' n []    = Con $ Builtin.tuple n
  tupleLit' n (h:t) = App (tupleLit' n t) h

listPat :: [Pat v] -> Pat v
listPat []    = ConP Builtin.nil []
listPat (h:t) = ConP Builtin.cons [h, listPat t]

consPat :: Pat v -> Pat v -> Pat v
consPat h t = ConP Builtin.cons [h, t]

strPat :: String -> Pat v
strPat = listPat . map (LitP . CharL)

tuplePat :: [Pat v] -> Pat v
tuplePat pats = ConP (Builtin.tuple $ length pats) pats

-------------------------------------------------------------------------------
-- Pretty-printing
instance (IsString v, Pretty v) => Pretty (Pat v) where
  prettyPrec ns d pat = case pat of
    VarP v    -> prettyPrec ns d v
    WildP     -> text "_"
    LitP c    -> prettyPrec ns d c
    ConP c [] -> prettyPrec ns d c
    ConP c ps -> prettyApps ns d (prettyPrecF c) (map prettyPrecF ps)

instance (Pretty t, Pretty v, IsString v) => Pretty (Expr t v) where
  prettyPrec ns d expr = case expr of
    Var v       -> prettyPrec ns d v
    Con c       -> prettyPrec ns d c
    Lit l       -> prettyPrec ns d l
    Let xs e    -> parensIf (d > letPrec) $
      prettyGenVars ns d (V.length xs) $ \ns' _ defNames def -> align $
        text "let" <+>
        align
        (vcat [prettyClauses ns' name mn mt cls def
                 | (name, (mn, mt, cls)) <- zip defNames $ V.toList xs]) <$$>
        indent 1 (text "in" <+>
        prettyPrec ns' letPrec (instantiate (return . def) e))
        where
          prettyClauses ns' name mn mt cls def = case mt of
            Just t -> namepart <+> text ":" <+> align (pretty ns' t) <$$>
                        indent 2 (vcat $ map prettyClause cls)
            Nothing -> namepart <+> align (vcat $ map prettyClause cls)
            where
              namepart = text name <> maybe mempty ((text "@" <>) . pretty ns') mn
              prettyClause (ps, s) = prettyGenVars ns' 0 numVars $ \ns'' _ _ pat ->
                hsep (map (prettyPrec ns'' (appPrec + 1) . fmap pat) ps) <+>
                text "=" <+>
                align (prettyPrec ns'' 0 $ instantiate (return . either pat def) s)
                where
                  numVars = length $ foldMap (foldMap (:[])) ps
    Lam s       -> prettyAbs ns d "\\" "." (\v -> instantiate1 (return v) s)
    App e e'    -> prettyApp ns d e e'
    Case e as   -> parensIf (d > casePrec) $
      text "case" <+> prettyPrec ns 0 e <+> text "of" <$$>
        indent 2 (align $ vcat $ map (prettyPrec ns d) as)

    Anno e t    -> parensIf (d > annoPrec) $
      prettyPrec ns annoPrec e <+> text ":" <+> prettyPrec ns annoPrec t
    AtNode e n  -> parensIf (d > atNodePrec) $
      prettyPrec ns atNodePrec e <> text "@" <> prettyPrec ns atNodePrec n

instance (Monad e, Pretty (e v), Pretty v, IsString v) => Pretty (Branch e v) where
  prettyPrec ns d (Branch p s) = prettyGenVars ns d numVars $ \ns' _ _ lu ->
    prettyPrec ns' 0 (fmap lu p) <+> text "->" <+>
    prettyPrec ns' 0 (instantiate (return . lu) s)
    where
      fvs     = foldMap (:[]) p
      numVars = length fvs
