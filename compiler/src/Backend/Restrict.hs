{-# LANGUAGE ViewPatterns #-}
module Backend.Restrict where

import qualified Bound
import Bound(fromScope, toScope)
import Bound.Var
import Control.Applicative
import Control.Monad
import Control.Monad.State
import Data.Foldable as F
import Data.List
import qualified Data.Map as M
import Data.Maybe
import Data.Monoid
import qualified Data.Traversable as T
import Data.Vector(Vector)
import qualified Data.Vector as V
import qualified Data.Set as S

import Backend.Core
import Backend.Prog
import qualified Branches
import Builtin.Arities(Arity)
import qualified Checker.Expr as Checker
import qualified Checker.Prog as Checker
import Class
import Constr
import SimpleScope
import Type(DataDef(..))
import Util
import Util.TopoSort

pass :: Checker.Prog -> Prog
pass (Checker.Prog defs _ constrs) = fromLifted "restricted"
    $ (def constrsf . Checker.removeTypeAnnos)
   <$> defs
  where
    constrsf :: ECon -> (Arity, Maybe Literal)
    constrsf con = (length ts, ml)
      where
        ml | null ts                                  = Just $ IntL l
           | length (filter isConRepresented cs) == 1 = Nothing
           | otherwise                                = Just $ IntL l
        (_, ts) = cs !! l
        l = fromMaybe unknown $ findIndex ((== con) . fst) cs
        (_, DataDef _ cs) = fromMaybe unknown $ M.lookup con constrs
        unknown = error $ "Don't know the arity of: " ++ show con
        isConRepresented (_, []) = False
        isConRepresented _       = True

def :: (ECon -> (Arity, Maybe Literal))
    -> Checker.Expr Empty EVar -> Lift (Maybe Node, Def EVar)
def constr (etaExpand (fst . constr) -> expr) = case expr of
  Checker.AtNode e n -> withNode (Just n) $ def constr e
  _ -> do
    mn <- gets currentNode
    fmap ((,) mn) $ case abssView expr of
      Just (n, s) -> (FunD n . Scope . fmap (unvar F B))
                 <$> body constr (unvar F B <$> fromScope s :: Checker.Expr Empty (Var EVar Int))
      Nothing     -> (ConstD . fmap (unvar id err)) 
                 <$> body constr (B <$> expr :: Checker.Expr Empty (Var EVar Empty))
        where
          err = error "Backend Restrict def"

-- | Eta-expand constructor applications (one layer)
etaExpand :: Eq v
          => (ECon -> Int)
          -> Checker.Expr Empty v -> Checker.Expr Empty v
etaExpand arityf (appsView -> (Checker.Con c, es))
  | numArgs < arity = expanded
  | numArgs > arity = tooMany
    where
      expanded = fmap (unvar err id)
               $ abss vs $ Class.apps (Checker.Con c) $
                                      (fmap F <$> es) ++ (return <$> vs)
        where
          vs = map B $ take (arity - numArgs) [(0 :: Int)..]
      numArgs = length es
      arity = arityf c
      tooMany = error $ "constructor applied to too many arguments: " ++ show c
      err = error "Backend Restrict conCase"
etaExpand arityf (Checker.Lam s) = Checker.Lam $ toScope $ etaExpand arityf $ fromScope s
etaExpand _      expr            = expr

body :: Ord v
     => (ECon -> (Arity, Maybe Literal))
     -> Checker.Expr Empty (Var EVar v) -> Lifted Body v
body constr (etaExpand (fst . constr) -> expr) = case appsView expr of
  (Checker.Var v, []) -> return $ Expr $ Var v
  (Checker.Var v, es) -> bcall (Expr $ Var v) =<< mapM (body constr) es
  (Checker.Con c, es) -> bcon l =<< mapM (body constr) es
    where
      (_, l) = constr c
  (Checker.Lit l, []) -> return $ Expr $ Con (Just l) []
  (Checker.Lit _, _ ) -> err
  (_,             []) -> case abssView expr of
    Just (n, s) -> do
      s' <- body constr $ fromScope' s
      Expr <$> liftFun n (scope' s') []
    Nothing     -> case expr of -- some cases should already be handled, but we totally have to be total
      Checker.Var _       -> err
      Checker.Con _       -> err
      Checker.Lit _       -> err
      Checker.Let e s     -> do
        b  <- body constr e
        s' <- body constr $ fromScope' s
        blet b (abctobac <$> s')
      Checker.LetRec xs s -> letRec constr xs s
      Checker.Lam _       -> err
      Checker.App _ _     -> err
      Checker.Case e brs  -> do
        b <- body constr e
        bcase b =<< branches brs
      Checker.Anno _ _    -> err
      Checker.AtNode e n  -> do
        mn <- gets currentNode
        if mn == Just n
          then body constr e
          else withNode (Just n) $ do
            b <- body constr e
            Expr <$> liftBody b
      Checker.FatBar e e' -> FatBar <$> body constr e <*> body constr e'
      Checker.Fail        -> return Fail
  (e,             es) -> do
    b <- body constr e
    bcall b =<< mapM (body constr) es
  where
    branches (Branches.ConBranches cbrs)     = ConBranches <$> sequence
      [ (ConBranch (snd $ constr c) n . scope')
          <$> body constr (fromScope' s)
      | Branches.ConBranch c n s <- cbrs]
    branches (Branches.LitBranches lbrs d) = LitBranches <$> sequence
      [ LitBranch l <$> body constr b | Branches.LitBranch l b <- lbrs]
      <*> body constr d
    err = error "Backend Restrict body"
    fromScope' = fmap abctobac . fromScope
    scope' = Scope . fmap abctobac

letRec :: Ord v
       => (ECon -> (Arity, Maybe Literal))
       -> Vector (LetScope (Checker.Expr Empty) (Var EVar v))
       -> LetScope (Checker.Expr Empty) (Var EVar v)
       -> Lifted Body v
letRec constr xs e = do
  names <- V.fromList <$> replicateM (V.length xs) freshName
  let inst    = return . B . (names V.!)
      instxs  = Bound.instantiate inst <$> xs
      xsfvs   = foldMap S.singleton <$> instxs
      isF (F _) = True
      isF _     = False
      deps    = fmap (map (unvar err id) . filter isF . S.toList)
              $ closure $ M.fromList $ V.toList $ V.zip (B <$> names) xsfvs
      subst n = maybe (return n) (apps (return n) . map (return . F)) $ M.lookup n deps
      xs'         = (>>= subst) <$> instxs
      close (n, x) = fmap (unvar id err) $ abss' (map F $ deps M.! B n) x
  ds <- T.mapM (def constr . close) $ V.zip names xs'
  modify $ \st -> st {lifted = M.fromList (V.toList $ V.zip names ds) <> lifted st}
  body constr (Bound.instantiate inst e >>= subst)
  where
    abss' vs (Checker.AtNode e n) = Checker.AtNode (abss vs e) n
    abss' vs e                    = abss vs e
    err = error "Backend Restrict letRec"
