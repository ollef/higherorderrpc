module Backend.Prog where

import Data.Map(Map)
import qualified Data.Map as M
import Data.Monoid
import Text.PrettyPrint.ANSI.Leijen(text, vcat, (<+>))
import Util(foldMapWithKey)

import Backend.Core
import Constr
import Pretty

data Prog = Prog
  { definitions :: Map EVar (Maybe Node, Def EVar)
  } deriving (Eq, Ord, Show)

instance Pretty Prog where
  pretty ns (Prog ds) = vcat
    [pretty ns n <> node mn <+> text "="
                <+> prettyFree d | (n, (mn, d)) <- M.toList ds]
    where
      node (Just x) = text "@" <> pretty ns x
      node Nothing  = mempty

fromLifted :: String -> Map EVar (Lift (Maybe Node, Def EVar)) -> Prog
fromLifted passName = Prog . foldMapWithKey go
  where
    go n l = M.singleton n d <> ls
      where 
        (d, ls) = runLift passName n l
