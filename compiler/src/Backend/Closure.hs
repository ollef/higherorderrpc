-- | Closure-conversion
module Backend.Closure where

import Bound.Var
import Control.Applicative
import qualified Data.Map as M

import Backend.Core
import Backend.Prog
import Backend.Runtime(closeFun, closeRemoteFun, remoteApplyKnown, remoteApplyKnown', lett)
import Builtin.Arities(Arity)
import qualified Builtin.Arities as Builtin
import qualified Builtin.Names as Builtin
import Control.Monad.State
import Constr
import SimpleScope
import Util(Empty)

pass :: Prog -> Prog
pass (Prog defs) = fromLifted "closure"
                 $ fmap (\(mn, d) -> (,) mn <$> withNode mn (definition arities d))
                        defs
  where
    arities :: EVar -> (Maybe Node, Maybe Arity)
    arities v = (,) (defNode =<< d) $
      (defArity =<< d) <|> Builtin.findVarArity v
      where
        d = M.lookup v defs
        defNode  (mn, _)        = mn
        defArity (_, FunD n _) = Just n
        defArity (_, ConstD _) = Nothing

definition :: (EVar -> (Maybe Node, Maybe Arity))
           -> Def EVar -> Lift (Def EVar)
definition arities def = case def of
  FunD n s -> do
    s' <- scope arities (fmap B s :: Scope Int Body (Var EVar Empty))
    return $ FunD n $ fmap (unvar id err) s'
  ConstD b -> do
    b' <- body arities (fmap B b :: Body (Var EVar Empty))
    return $ ConstD $ fmap (unvar id err) b'
  where
    err = error "Backend Closure pass"

closure :: (Eq b, Eq v) => EVar -> Int -> [Var b (Var EVar v)] -> Body (Var b (Var EVar v))
closure f i vs = lett arity (Con (Just $ IntL i) [])
               $ lett f' (Call (F $ B $ Builtin.tagFunPtr) [F $ B f])
               $ lett cl (Con Nothing $ f' : fid : arity : vs)
               $ Expr $ Var cl
  where
    arity = F $ B $ EVar "__arity"
    f'    = F $ B $ EVar "__f'"
    fid   = F $ B $ Builtin.idOf f
    cl    = F $ B $ EVar "__cl"

apply :: (Eq v, Eq b) => Expr (Var b (Var EVar v)) -> [Var b (Var EVar v)] -> Body (Var b (Var EVar v))
apply e vs = lett x e
           $ lett app (Call (F $ B $ Builtin.apply $ length vs) (x : vs))
           $ Expr $ Var app
  where
    x   = F $ B $ EVar "__x"
    app = F $ B $ EVar "__app"

variable :: Ord v
         => (EVar -> (Maybe Node, Maybe Arity))
         -> Var EVar v -> Lifted Body v
variable arities (B v) = case arities v of
  (_, Just _)  -> expression arities $ Call (B v) []
  (_, Nothing) -> return $ Expr $ Var $ B v
variable _ v = return $ Expr $ Var v

expression :: Ord v
           => (EVar -> (Maybe Node, Maybe Arity))
           -> Expr (Var EVar v) -> Lifted Body v
expression arities expr = case expr of
  Var v    -> variable arities v
  Con c vs -> do
    bs <- mapM (variable arities) vs
    blets bs $ Expr $ Con c $ take (length vs) $ map B [0..]
  Call x vs -> do
    bs <- mapM (variable arities) vs
    marity <- canFastCall x
    case marity of
      Just (arity, Left (f, immobile))
        | nargs == arity -> blets bs $ Expr $ Call (F $ B f) vs0
        | nargs  < arity -> do
            -- mn <- gets currentNode
            let want = arity - nargs
            if immobile then do
              cl <- liftClosedDef Nothing $ closeRemoteFun nargs want (Builtin.nodeOf f) (Builtin.idOf f)
              blets bs $ closure cl want vs0
            else do
              cl <- liftClosedDef Nothing $ closeFun nargs want f
              blets bs $ closure cl want vs0
        | otherwise      -> blets bs $ apply (Call (F $ B f) vsarity) vsrest
        where
          (vsarity, vsrest) = splitAt arity vs0
      Just (arity, Right (remap, fid, fnode))
        | nargs == arity -> blets bs $ abctobac <$> remap fnode fid vs0
        | nargs  < arity -> do
            let want = arity - nargs
            cl <- liftClosedDef Nothing $ closeRemoteFun nargs want fnode fid
            blets bs $ closure cl want vs0
        | otherwise      -> do
          blets bs =<< fmap abctobac <$> blet (remap fnode fid vsarity)
                                              (apply (Var $ B ()) $ fmap (F . F) vsrest)
        where
          vsarity, vsrest :: [Var Int v]
          (vsarity, vsrest) = splitAt arity vs0
      Nothing -> blets bs $ apply (Var $ F x) vs0
    where
      nargs = length vs
      vs0 :: [Var Int v]
      vs0 = take nargs $ map B [(0 :: Int)..]
      canFastCall (F _) = return Nothing
      canFastCall (B f) = do
        myNode <- gets currentNode
        let (mn, ar) = arities f
        return $ case (ar, mn, myNode) of
          (Just arity, Just n, Just n')
            | n == n'   -> Just (arity, Left (f, True))
            | otherwise -> Just (arity, Right (remoteApplyKnown, Builtin.idOf f, Builtin.nodeOf f))
          (Just arity, Just _, Nothing) ->
            Just (arity, Right (remoteApplyKnown', Builtin.idOf f, Builtin.nodeOf f))
          (Just arity, Nothing, _) -> Just (arity, Left (f, False))
          _                        -> Nothing

body :: Ord v
     => (EVar -> (Maybe Node, Maybe Arity))
     -> Body (Var EVar v) -> Lifted Body v
body arities bod = case bod of
  Let e s      -> do
    b <- expression arities e
    blet' b =<< scope arities s
  Case v brs   -> Case v <$> branches brs
  Fail         -> return Fail
  FatBar b1 b2 -> FatBar <$> body arities b1 <*> body arities b2
  Expr e       -> expression arities e
  where
    branches (ConBranches cbrs) = ConBranches <$> sequence
      [ConBranch c n <$> scope arities s | ConBranch c n s <- cbrs]
    branches (LitBranches lbrs d) = LitBranches <$> sequence
      [LitBranch l <$> body arities b | LitBranch l b <- lbrs]
      <*> body arities d

scope :: (Ord v, Ord b)
      => (EVar -> (Maybe Node, Maybe Arity))
      -> Scope b Body (Var EVar v) -> Lifted (Scope b Body) v
scope arities (Scope x) = do
  x' <- body arities $ abctobac <$> x
  return $ Scope $ abctobac <$> x'
