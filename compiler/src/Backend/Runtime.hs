-- | Generate code for some runtime system functions
module Backend.Runtime where

import Bound.Var
import Control.Applicative
import qualified Data.Map as M
import qualified Data.Vector as V

import Backend.Core
import Backend.Prog
import Builtin.Arities(Arity)
import qualified Builtin.Names as Builtin
import Constr
import SimpleScope

maxArity :: Arity
maxArity = 16 -- ought to be enough for anybody

runtimeFunctions :: Prog
runtimeFunctions = Prog $ M.fromList $
  [(Builtin.apply n, (Nothing, apply n)) | n <- [0..maxArity]] ++
  [(Builtin.pap k m, (Nothing, pap k m)) | m <- [0..maxArity], k <- [0 .. maxArity - m]] ++
  [(Builtin.remoteApply n, (Nothing, remoteApply n)) | n <- [0 .. maxArity]] ++
  [(Builtin.remoteApplyReceive, (Nothing, remoteApplyReceive))]

mobileRuntimeFunctions :: [EVar]
mobileRuntimeFunctions = concat
  [ [Builtin.pap k m | m <- [0 .. maxArity], k <- [0 .. maxArity - m]]
  , [Builtin.remoteApply n | n <- [0 .. maxArity]]
  , Builtin.primitiveFunctions
  ]

lett :: Eq v => v -> Expr v -> Body v -> Body v
lett v e b = Let e $ abstract1 v b

funD :: [EVar] -> Body EVar -> Def EVar
funD ns b = FunD (V.length vns) $ abstract (`V.elemIndex` vns) b
  where
    vns = V.fromList ns

conCase :: EVar -> [(Maybe Literal, [EVar], Body EVar)] -> Body EVar
conCase v brs = Case v $ ConBranches [ConBranch c (V.length vvs) $ abstract (`V.elemIndex` vvs) b
                                     | (c, vs, b) <- brs, let vvs = V.fromList vs
                                     ]

litCase :: v -> [(Literal, Body v)] -> Body v -> Body v
litCase v brs d = Case v $ LitBranches (map (uncurry LitBranch) brs) d

apply :: Int -> Def EVar
apply m = funD (this : vs) $ conCase this
  [(Nothing, [f, fid, nv], lett f' (Call Builtin.untagFunPtr [f]) $
                      litCase nv [(IntL i, branch  i) | i <- [0..maxArity]] Fail)]
  where
    branch n
      | m == n    = Expr $ Call f' $ this : vs
      | m <  n    = lett nm (Con (Just $ IntL $ n - m) []) $
                    lett p (Call Builtin.tagFunPtr [Builtin.pap (n - m) m]) $
                    Expr $ Con Nothing $ p : papid (n - m) m : nm : this : vs
      | otherwise = lett fvsn (Call f' $ this : vsn) $ Expr $
                      Call (Builtin.apply $ m - n) (fvsn : vsm)
        where
          (vsn, vsm) = splitAt n vs

    this = EVar "this"
    vs   = [EVar $ "x" ++ show i | i <- [1 .. m]]
    f    = EVar "f"
    fid  = EVar "fid"
    f'   = EVar "f'"
    p    = EVar "pap"
    nv   = EVar "n"
    nm   = EVar "nm"
    fvsn = EVar "fvsn"
    papid x y = Builtin.idOf (Builtin.pap x y)

pap :: Int -> Int -> Def EVar
pap k m = funD (this : ks) $ conCase this
  [(Nothing, (wild1 : wild2 : wild3 : that : ms), Expr $ Call (Builtin.apply $ k + m)
                                                              (that : ms ++ ks))]
  where
    this = EVar "this"
    that = EVar "that"
    wild1 = EVar "unused1"
    wild2 = EVar "unused2"
    wild3 = EVar "unused3"
    ks   = [EVar $ "k" ++ show i | i <- [1 .. k]]
    ms   = [EVar $ "m" ++ show i | i <- [1 .. m]]

-- | Construct the initial pap for an under-applied function
closeFun :: Int -> Int -> EVar -> Def EVar
closeFun have want f = funD (this : wantvs) $
  conCase this [(Nothing, wild1 : wild2 : wild3 : havevs, Expr $ Call f (havevs ++ wantvs))]
  where
    this   = EVar "__this"
    wild1  = EVar "__unused1"
    wild2  = EVar "__unused2"
    wild3  = EVar "__unused3"
    wantvs = [EVar $ "__x" ++ show i | i <- [1..want]]
    havevs = [EVar $ "__y" ++ show i | i <- [1..have]]

-- | Construct the initial pap for an under-applied remote function
closeRemoteFun :: Int -> Int -> EVar -> EVar -> Def EVar
closeRemoteFun have want fnode fid = funD (this : wantvs) $
  conCase this [(Nothing, wild1 : wild2 : wild3 : havevs, unvar id id <$> remoteApplyKnown' fnode fid (havevs ++ wantvs))]
  where
    islocal = EVar "__islocal"
    f       = EVar "__f"
    f'      = EVar "__f'"
    this    = EVar "__this"
    wild1   = EVar "__unused1"
    wild2   = EVar "__unused2"
    wild3   = EVar "__unused3"
    wantvs  = [EVar $ "__x" ++ show i | i <- [1..want]]
    havevs  = [EVar $ "__y" ++ show i | i <- [1..have]]

remoteApplyKnown' :: Eq v => EVar -> EVar -> [v] -> Body (Var EVar v)
remoteApplyKnown' node fid vs
  = lett islocal (Call (B Builtin.isLocalNode) [B node])
  $ litCase islocal
  [(IntL 0, remoteApplyKnown node fid vs)
  ,(IntL 1, lett f (Call (B Builtin.lookupFunction) [B fid])
          $ lett f' (Call (B Builtin.untagFunPtr) [f])
          $ Expr $ Call f' (map F vs))
  ] Fail
  where
    islocal = B $ EVar "__islocal"
    f       = B $ EVar "__f"
    f'      = B $ EVar "__f'"

-- | Call a known remote function (must be saturated)
remoteApplyKnown :: Eq v => EVar -> EVar -> [v] -> Body (Var EVar v)
remoteApplyKnown node fid vs
  = lett n (Con (Just $ IntL $ length vs) [])
  $ lett cl (Con Nothing $ B fid : n : vs')
  $ Expr $ Call (B Builtin.remoteApplySend) [B node, cl]
  where
    vs'      = map F vs
    n        = B $ EVar "__nvar"
    cl       = B $ EVar "__closure"

-- * Sending and receiving remote function pointers
remoteApply :: Int -> Def EVar
remoteApply m = funD (this : vs) $ conCase this
  [(Nothing, [wild1, wild2, wild3, fid, node], unvar id id <$> remoteApplyKnown node fid vs)]
  where
    wild1 = EVar "__unused1"
    wild2 = EVar "__unused1"
    wild3 = EVar "__unused1"
    fid   = EVar "__fid"
    this  = EVar "__this"
    node  = EVar "__node"
    vs    = [EVar $ "__x" ++ show i | i <- [1 .. m]]

remoteApplyReceive :: Def EVar
remoteApplyReceive = funD [this] $
  conCase this
    [(Nothing, [fid, nv], lett f (Call Builtin.lookupFunction [fid]) $
                          lett f' (Call Builtin.untagFunPtr [f]) $
                            litCase nv [(IntL n, branch n) | n <- [0..maxArity]] Fail
    )]
  where
    branch n = conCase this
      [(Nothing, dummy1 : dummy2 : vs, Expr $ Call f' vs)]
      where
        vs = [EVar $ "__x" ++ show i | i <- [1..n]]
    this  = EVar "__this"
    dummy1 = EVar "__unused1"
    dummy2 = EVar "__unused2"
    f    = EVar "__f"
    f'   = EVar "__f'"
    fid  = EVar "__fid"
    nv   = EVar "__n"
