module Backend.CodeGen where

import Bound.Var
import Control.Applicative
import Control.Monad
import Data.Bifoldable
import Data.Bits
import Data.Char
import Data.Foldable(foldMap)
import Data.List
import Data.Map(Map)
import qualified Data.Map as M
import Data.Maybe
import Data.Monoid
import qualified Data.Set as S
import Data.Text.Lazy(Text)
import Data.Text.Lazy.Builder
import qualified Data.Vector as V

import Backend.Core
import Backend.Prog
import Backend.Runtime
import qualified Builtin.Names as Builtin
import Constr
import SimpleScope
import Util.TopoSort

infixl 9 !
infix 1 =:
infixr 6 <+>

newtype CVar = CVar {unCVar :: String}
  deriving (Eq, Ord, Show)

ckeyPrefix, primedPrefix :: String
ckeyPrefix = "_ckeyword_"
primedPrefix = "_primed_"

-- | Inject 'EVar's into 'CVar's.
escape :: EVar -> CVar
escape (EVar "main") = CVar "_main"
escape (EVar s)
  | s `S.member` ckeywords = CVar $ ckeyPrefix ++ s
  | null primes = CVar s
  | otherwise   = CVar $ primedPrefix
                      ++ intercalate "_" (map show primes)
                      ++ "_" ++ unprimed
  where
    primes   = elemIndices '\'' s
    unprimed = filter (/= '\'') s

ckeywords :: S.Set String
ckeywords = S.fromList
  [ "auto"    , "break"   , "case"    , "char"
  , "const"   , "continue", "default" , "do"
  , "double"  , "else"    , "enum"    , "extern"
  , "float"   , "for"     , "goto"    , "if"
  , "int"     , "long"    , "register", "return"
  , "short"   , "signed"  , "sizeof"  , "static"
  , "struct"  , "switch"  , "typedef" , "union"
  , "unsigned", "void"    , "volatile", "while"
  ]

wordSize :: Int
wordSize = 8

type CProg = Builder

str :: String -> Builder
str = fromString

var :: EVar -> Builder
var s = str $ unCVar $ escape s

(<+>) :: Builder -> Builder -> Builder
b <+> b' = b <> str" " <> b'

tint :: Int -> CProg
tint n = str $ show (shiftL n 1 + 1)

tchar :: Char -> CProg
tchar c = tint (ord c)

lit :: Literal -> CProg
lit (IntL i) = tint i
lit (CharL c) = tchar c

tupled :: [CProg] -> CProg
tupled xs = str"(" <> mconcat (intersperse (str", ") xs) <> str")"

(!) :: CProg -> Int -> CProg
x ! n = x <> str"[" <> str (show n) <> str"]"

indent :: [CProg] -> [CProg]
indent = map (str"\t" <>)

scope :: [CProg] -> [CProg]
scope cs = [str"{"] ++ indent cs ++ [str"}"]

dval :: CProg -> CProg
dval x = str"Value" <+> x

dvalptr :: CProg -> CProg
dvalptr x = str"Value*" <+> x

valcast :: CProg -> CProg
valcast x = str"((Value)" <+> x <> str")"

valptrcast :: CProg -> CProg
valptrcast x = str"((Value*)" <+> x <> str")"

funcast :: Int -> CProg -> CProg
funcast n x = str"((Value(*)" <> tupled (replicate n (str"Value")) <> str")(" <> x <> str"))"

(=:) :: CProg -> CProg -> CProg
x =: y = x <+> str"=" <+> y <> str";"

malloc :: CProg
malloc = str"Malloc"

call :: CProg -> [CProg] -> CProg
call f xs = f <> tupled xs

switch :: CProg -> [(CProg, [CProg])] -> Maybe [CProg] -> [CProg]
switch x cs md = [str"switch(" <> x <> str")"] ++
  scope (concat [[str"case" <+> c <> str":"] ++
                       scope (stmts ++ [str"break;"])
                | (c, stmts) <- cs]
          ++ maybe [] (\d -> [str"default:"] ++ scope (d ++ [str"break;"])) md
        )

-- TODO this is a mess
pass :: Prog -> [(Node, Text)]
pass p@(Prog ds) = generateNodes funids consts p
  where
    allNodes     = S.fromList $ catMaybes $ fst <$> M.elems ds
    funidslist   = map nodeTable $ S.toList allNodes
    funids       = ((mobileRuntimeFunctions ++ mobileFuns) ++) <$> M.fromList funidslist
    mobileFuns   = M.keys $ M.filter ((== Nothing) . fst) ds
    nodeTable n  = (n, M.keys (M.filter ((==) (Just n) . fst) ds))
    numMobileRTFuns = length mobileRuntimeFunctions
    mobileCutoff = numMobileRTFuns + length mobileFuns
    consts       = zip (map Builtin.idOf mobileFuns) [numMobileRTFuns..]
                ++ concat [[(Builtin.idOf f, i), (Builtin.nodeOf f, n)]
                          | (n, (_, fs)) <- zip [0..] funidslist
                          , (f, i) <- zip fs [mobileCutoff..]]

sortProg :: Prog -> ([(EVar, Int, Scope Int Body EVar)], [[(EVar, Body EVar)]])
sortProg (Prog ds) = (fs, sortedcs)
  where
    dsl = M.toList ds
    fs = [(name, n, s) | (name, (_, FunD n s)) <- dsl]
    cs = M.fromList [(name, b) | (name, (_, ConstD b)) <- dsl]
    sortedcs = [[(n, fromJust look) | n <- names, let look = M.lookup n cs, isJust look]
               | names <- topoSort $ (bifoldMap mempty $ foldMap S.singleton) <$> ds
               ]

generateNodes :: Map Node [EVar] -> [(EVar, Int)] -> Prog -> [(Node, Text)]
generateNodes funids consts p =
  [(node, generateNode (nodeFunTable node) consts $ nodeProg node p)
  | node <- allNodes]
  where
    nodeFunTable = (funids M.!)
    allNodes = M.keys funids
    nodeProg :: Node -> Prog -> Prog
    nodeProg n (Prog ds) = Prog $ M.filter (maybe True (== n) . fst) ds

generateNode :: [EVar] -> [(EVar, Int)] -> Prog -> Text
generateNode ftable consts p
  = toLazyText
  $ mconcat
  $ intersperse (str"\n")
  $ makeProg
  $ foldMap (uncurry3 funDef) fs
  <> foldMap constDefs cs
  where
    (fs, cs) = sortProg p
    uncurry3 f (x, y, z) = f x y z
    makeProg (decls, inMain, defs) = concat
      [ [str"#include \"runtime/runtime.h\""]
      , [str"#include \"runtime/closure.h\""]
      , decls
      , [str"const" <+> dval (var v) =: tint n | (v, n) <- consts]
      , functionTable ftable
      , [str"int main(int argc, char** argv)"]
      , scope $ [str"InitRuntime(argc, argv);"] ++ inMain ++ [str"MainLoop();"]
      , defs
      ]

closureFile :: Prog -> Text
closureFile (Prog ds) = generateRuntime (length mobileFuns) runtimeFunctions
  where
    mobileFuns   = M.keys $ M.filter ((== Nothing) . fst) ds

generateRuntime :: Int -> Prog -> Text
generateRuntime numMobileFuns p
  = toLazyText
  $ mconcat
  $ intersperse (str"\n")
  $ makeProg
  $ foldMap (uncurry3 funDef) fs
  <> foldMap constDefs cs
  where
    mobileCutoff = length mobileRuntimeFunctions + numMobileFuns
    consts       = zip (map Builtin.idOf mobileRuntimeFunctions) [0..]
    (fs, cs) = sortProg p
    uncurry3 f (x, y, z) = f x y z
    makeProg (decls, _, defs) = concat
      [ decls
      , [str"const size_t" <+> var Builtin.mobileCutoff =: str (show mobileCutoff)]
      , [str"const" <+> dval (var v) =: tint n | (v, n) <- consts]
      , defs
      ]

functionTable :: [EVar] -> [CProg]
functionTable xs =
  [str"const" <+> dval (var Builtin.functionTable) <> str"[]" <+> str"="] ++
  scope [mconcat $ intersperse (str", ") [valcast (str"&" <> var x) | x <- xs]] ++
  [str";"]

constDefs :: [(EVar, Body EVar)] -> ([CProg], [CProg], [CProg])
constDefs []          = mempty
constDefs [(name, b)] = constDef name b
constDefs xs          = error $ "circular top-level constants: " ++ cs
  where
    cs = intercalate ", " $ map (raw . fst) xs
    raw (EVar s) = s

constDef :: EVar -> Body EVar -> ([CProg], [CProg], [CProg])
constDef name b =
  ( [ functionDecl (constName name) 0
    , constDecl name
    ]
  , expression (Call (constName name) []) (\c -> [var name =: c])
  , function vs' (constName name) 0 (Scope $ F <$> b)
  )
  where
    constName (EVar n) = EVar $ "_const_" ++ n
    fvs = foldMap S.singleton b
    vs' = filter (`S.notMember` fvs) vs
    vs = do
      i <- [(0 :: Int)..]
      v <- ['a'..'z']
      return (EVar $ [v] ++ if i == 0 then "" else show i)

funDef :: EVar -> Int -> Scope Int Body EVar -> ([CProg], [CProg], [CProg])
funDef name n s = ([functionDecl name n], [], function vs' name n s)
  where
    fvs = foldMap S.singleton s
    vs' = filter (`S.notMember` fvs) vs
    vs = do
      i <- [(0 :: Int)..]
      v <- ['a'..'z']
      return (EVar $ [v] ++ if i == 0 then "" else show i)

constDecl :: EVar -> CProg
constDecl name = dval (var name) <> str";"

functionDecl :: EVar -> Int -> CProg
functionDecl name n =
  dval (var name) <> tupled (replicateM n str"Value") <> str";"

function :: [EVar] -> EVar -> Int -> Scope Int Body EVar -> [CProg]
function vs name n s = concat
  [ [dval (var name) <> tupled [dval $ var bv | bv <- bvs]]
  , scope $ body vs' $ instantiate (vbvs V.!) s
  ]
  where
    (bvs, vs') = splitAt n vs
    vbvs = V.fromList bvs

expression :: Expr EVar -> (CProg -> [CProg]) -> [CProg]
expression expr k = case expr of
  Var v            -> k $ var v
  Con Nothing []   -> k $ tint 0
  Con Nothing vs   -> scope $ concat
    [ [dvalptr con =: call malloc [str $ show $ length vs * wordSize]]
    , [con ! n =: valcast (var v) | (n, v) <- zip [0..] vs]
    , k $ valcast con
    ]
  Con (Just l) []  -> k $ lit l
  Con (Just l) vs  -> scope $ concat
    [ [dvalptr con =: call malloc [str $ show $ (length vs + 1) * wordSize]]
    , [con ! 0 =: lit l]
    , [con ! n =: valcast (var v) | (n, v) <- zip [1..] vs]
    , k $ valcast con
    ]
  Call v vs        -> k $ (funcast (length vs) $ var v) <> tupled (map (valcast . var) vs)
  where
    con = str"__con"

body :: [EVar] -> Body EVar -> [CProg]
body vs bod = case bod of
  Case v brs  -> branches vs v brs
  Let e s     -> concat
    [ [dval (var v) <> str";"]
    , expression e (\c -> [var v =: c])
    , body vs' $ instantiate1 v s
    ]
    where
      v:vs' = vs
  FatBar b b' -> body vs b <> body vs b'
  Fail        -> [] -- switch will kindly put a break after every case
  Expr e      -> expression e (\c -> [str"return" <+> c <> str";"])

branches :: [EVar] -> EVar -> Branches Body EVar -> [CProg]
branches vs v brs = case brs of
  ConBranches [ConBranch Nothing n s] -> concat
    [ [ dval (var v') =: valptrcast (var v) ! i | (i, v') <- zip [0..] bvs]
    , body vs' $ instantiate (vbvs V.!) s
    ]
    where
      (bvs,vs') = splitAt n vs
      vbvs      = V.fromList bvs
  ConBranches cbrs   -> case partition isLitBranch cbrs of
    ([],   [])    -> []
    ([],   cbrs') -> switch (valptrcast (var v) ! 0) (map conbranch cbrs')
                            Nothing
    (lbrs, cbrs') -> body vs $ Case v $ LitBranches (map toLitBranch lbrs)
                                      $ Case v $ ConBranches cbrs'
  LitBranches lbrs d -> switch (var v)
                          [(lit l, body vs b) | LitBranch l b <- lbrs]
                          (Just $ body vs d)
  where
    isLitBranch (ConBranch _ 0 _) = True
    isLitBranch _                 = False
    toLitBranch (ConBranch (Just l) 0 s) = LitBranch l $ instantiate err s
    toLitBranch _ = err
    err = error "Backend CodeGen branches"

    conbranch (ConBranch (Just l) n s) = (,) (lit l) $ concat
      [ [ dval (var v') =: valptrcast (var v) ! i | (i, v') <- zip [1..] bvs]
      , body vs' $ instantiate (vbvs V.!) s
      ]
      where
        (bvs, vs') = splitAt n vs
        vbvs       = V.fromList bvs
    conbranch _ = err
