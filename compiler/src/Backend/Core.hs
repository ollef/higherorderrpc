{-# LANGUAGE DeriveFoldable, DeriveFunctor, DeriveTraversable, EmptyDataDecls, FlexibleContexts, FlexibleInstances, MultiParamTypeClasses #-}
module Backend.Core where

import Bound.Var
import Control.Applicative
import Control.Monad.State
import Data.Bifunctor
import Data.Foldable as F
import Data.Map(Map)
import qualified Data.Map as M
import Data.Maybe
import Data.Monoid
import qualified Data.Set as S
import Data.String
import Data.Traversable as T
import qualified Data.Vector as V
import Prelude.Extras
import Text.PrettyPrint.ANSI.Leijen(hsep, tupled, align, indent, text, vcat, (<+>), (<$$>), (</>))

import Constr
import Pretty
import SimpleScope

data Def v
  = FunD Int (Scope Int Body v)
  | ConstD (Body v)
  deriving (Eq, Ord, Show, Functor, Foldable, Traversable)

data Body v
  = Case v (Branches Body v)
  | Let (Expr v) (Scope1 Body v)
  | FatBar (Body v) (Body v)
  | Fail
  | Expr (Expr v)
  deriving (Eq, Ord, Show, Functor, Foldable, Traversable)
instance Eq1 Body
instance Ord1 Body
instance Show1 Body

data Expr v
  = Var v
  | Con (Maybe Literal) [v] -- ^ Nothing for constructors of single-constructor types,
                            --   must be fully applied
  | Call v [v]
  deriving (Eq, Ord, Show, Functor, Foldable, Traversable)
instance Eq1 Expr
instance Ord1 Expr
instance Show1 Expr

data Branches b v
  = ConBranches [ConBranch b v]       -- ^ Must be total
  | LitBranches [LitBranch b v] (b v) -- ^ Has default branch
  deriving (Eq, Ord, Show, Functor, Foldable, Traversable)

data ConBranch b v = ConBranch (Maybe Literal) Int (Scope Int b v)
  deriving (Eq, Ord, Show, Functor, Foldable, Traversable)

data LitBranch b v = LitBranch Literal (b v)
  deriving (Eq, Ord, Show, Functor, Foldable, Traversable)

type LBinding = Either Int Int -- Left for constant, Right for function

data LiftedState = LiftedState
  { lifted      :: Map EVar (Maybe Node, Def EVar)
  , currentNode :: Maybe Node
  , freshNames  :: [EVar]
  }

freshName :: Lift EVar
freshName = do
  n:ns <- gets freshNames
  modify $ \s -> s {freshNames = ns}
  return n

withNode :: Maybe Node -> Lift a -> Lift a
withNode n l = do
  orig <- gets currentNode
  modify $ \s -> s {currentNode = n}
  res <- l
  modify $ \s -> s {currentNode = orig}
  return res

type Lift = State LiftedState
type Lifted b v = Lift (b (Var EVar v))

runLift :: String -> EVar -> Lift a -> (a, Map EVar (Maybe Node, Def EVar))
runLift passName (EVar name) x = second lifted
                      $ runState x (LiftedState mempty Nothing ns)
  where
    ns = map EVar ["_lift_" ++ passName ++ "_" ++ name ++ "_" ++ show y | y <- [(0 :: Int)..]]

liftClosedDef :: Maybe Node -> Def EVar -> Lift EVar
liftClosedDef mn d = do
  name <- freshName
  modify $ \st -> st {lifted = M.insert name (mn, d) $ lifted st}
  return name

liftFun :: Ord v
        => Int
        -> Scope Int Body (Var EVar v) -> [Var EVar v]
        -> Lifted Expr v
liftFun n s@(Scope b) args = do
  mn <- gets currentNode
  name <- liftClosedDef mn $ FunD (numfvs + n) $ Scope $ f <$> b
  return $ Call (B name) $ map F lfvs ++ args
  where
    fvs = foldMap (foldMap S.singleton) s
    lfvs = S.toList fvs
    vfvs = V.fromList lfvs
    numfvs = V.length vfvs
    -- f :: Var Int (Var EVar v) -> Var Int EVar
    f (B n')    = B $ n' + numfvs
    f (F (B x)) = F x
    f (F (F v)) = B $ fromMaybe err $ V.elemIndex v vfvs
    err = error "Backend Core liftFun"

liftBody :: Ord v => Body (Var EVar v) -> Lifted Expr v
liftBody b = liftFun 0 (Scope $ F <$> b) []

abctobac :: Var a (Var b c) -> Var b (Var a c)
abctobac = unvar (F . B) (unvar B (F . F))

-------------------------------------------------------------------------------
-- * Smart constructors
elet :: Expr v -> Body (Var () v) -> Body v
elet (Var v) b                   = unvar (const v) id <$> b
elet e       (Expr (Var (B ()))) = Expr e
elet e       b                   = Let e $ Scope b

elet' :: Expr v -> Scope1 Body v -> Body v
elet' x = elet x . unScope

elets :: [Expr v] -> Body (Var Int v) -> Body v
elets []  b = unvar err id <$> b
  where err = error "Backend Core elets"
elets (e:es) b  = elet e (elets (fmap F <$> es) (f <$> b))
  where
  f (B 0) = F (B ())
  f (B n) = B $! n - 1
  f (F v) = F $ F v

econ :: Maybe Literal -> [Expr v] -> Body v
econ c es = elets es $ Expr $ Con c $ take (length es) $ map B [0..]

ecall :: Expr v -> [Expr v] -> Body v
ecall e es = elets (e:es) $ Expr $ Call (B 0) $ take (length es) $ map B [1..]

blet :: Ord v => Body (Var EVar v) -> Body (Var () (Var EVar v)) -> Lifted Body v
blet body body' = case body of
  --     let x = (let y = e in s) in s'
  -- ==> let y = e in (let x = s in s')
  Let e (Scope s) -> do
    inner <- blet (abctobac <$> s) (fmap (fmap F) <$> body')
    return $ elet e (abctobac <$> inner)
  --     let x = case v of brs in s
  -- ==> toplevel f fvs = case v of brs
  --     let x = f fvs in s
  Case v brs -> do
    e <- liftBody (Case v brs)
    return $ elet e body'
  Fail -> return Fail
  FatBar b1 b2 -> do
    e <- liftBody $ FatBar b1 b2
    return $ elet e body'
  Expr e -> return $ elet e body'

blet' :: Ord v => Body (Var EVar v) -> Scope1 Body (Var EVar v) -> Lifted Body v
blet' x = blet x . unScope

blets :: Ord v
      => [Body (Var EVar v)]
      -> Body (Var Int (Var EVar v)) -> Lifted Body v
blets []  b = return $ unvar err id <$> b
  where err = error "Backend Core blets"
blets (b:bs) b' = blets bs =<< (fmap abctobac <$> blet (fmap F <$> b) (f <$> b'))
  where
    f :: Var Int (Var EVar v) -> Var () (Var EVar (Var Int v))
    f (B 0)      = B ()
    f (B n)      = F $ F $ B $! n - 1
    f (F (B ev)) = F $ B ev
    f (F (F v))  = F $ F $ F v

bcon :: Ord v
     => Maybe Literal
     -> [Body (Var EVar v)] -> Lifted Body v
bcon c bs = blets bs
          $ Expr $ Con c $ take (length bs) $ map B [0..]

bcall :: Ord v
      => Body (Var EVar v)
      -> [Body (Var EVar v)] -> Lifted Body v
bcall b bs = blets (b:bs)
           $ Expr $ Call (B 0) $ take (length bs) $ map B [1..]

bcase :: Ord v
      => Body (Var EVar v)
      -> Branches Body (Var EVar v) -> Lifted Body v
bcase b brs = blet b $ Case (B ()) $ F <$> brs

--------------------------------------------------------------------------------
-- Pretty printing
instance (Pretty v, IsString v) => Pretty (Def v) where
  prettyPrec ns d (FunD numVars s) = prettyGenVars ns d numVars $ \ns' d' vs vars ->
      text "\\" <> hsep (map text vs) <> text "." <+>
                   prettyPrec ns' d' (instantiate vars s)
  prettyPrec ns d (ConstD b) = prettyPrec ns d b

instance (Pretty v, IsString v) => Pretty (Body v) where
  prettyPrec ns d body = case body of
    Case v brs -> parensIf (d > casePrec) $
      text "case" <+> pretty ns v <+> text "of" <$$>
        indent 2 (align $ prettyPrec ns d brs)
    Let e s -> parensIf (d > letPrec) $
      prettyGenVars ns d 1 $ \ns' _ [defName] def -> align $
        align (text "let" <+> text defName <+> text "=" <+> pretty ns' e) <$$>
        prettyPrec ns' letPrec (instantiate1 (def 0) s)
    Expr e -> prettyPrec ns d e
    FatBar b b' -> parensIf (d > fatPrec) $
      prettyPrec ns fatPrec b </> text "][" <+> prettyPrec ns (fatPrec + 1) b'
    Fail -> text "$FAIL"

instance (Pretty v, IsString v) => Pretty (Expr v) where
  prettyPrec ns d expr = case expr of
    Var v    -> prettyPrec ns d v
    Con Nothing [] -> text "(void)"
    Con Nothing vs -> parensIf (d > appPrec) $
      text "Con" <+> tupled (map (prettyPrec ns (appPrec + 1)) vs)
    Con (Just l) [] -> parensIf (d > appPrec) $
      text "Con" <+> prettyPrec ns appPrec l
    Con (Just l) vs -> parensIf (d > appPrec) $
      text "Con" <+>
      prettyPrec ns appPrec l <+> tupled (map (prettyPrec ns (appPrec + 1)) vs)
    Call v vs -> parensIf (d > appPrec) $
      prettyPrec ns appPrec v <+> tupled (map (prettyPrec ns (appPrec + 1)) vs)

instance (Functor e, Pretty (e v), Pretty v, IsString v) => Pretty (Branches e v) where
  prettyPrec ns d (ConBranches cbrs)     = vcat $ map (prettyPrec ns d) cbrs
  prettyPrec ns d (LitBranches lbrs def) =
       vcat $ map (prettyPrec ns d) lbrs
    ++ [text "_" <+> text "->" <+> prettyPrec ns 0 def]

instance (Functor e, Pretty (e v), Pretty v, IsString v) => Pretty (ConBranch e v) where
  prettyPrec ns d (ConBranch c numVars s) = prettyGenVars ns d numVars $ \fvs _ bvs lu ->
    pretty fvs (Con c $ map text bvs) <+>
    text "->" <+> align (pretty fvs $ instantiate lu s)

instance (Pretty (e v), Pretty v, IsString v) => Pretty (LitBranch e v) where
  prettyPrec ns _ (LitBranch l e) =
    pretty ns l <+> text "->" <+> align (pretty ns e)
