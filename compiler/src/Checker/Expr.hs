{-# LANGUAGE DeriveFoldable, DeriveFunctor, DeriveTraversable, ViewPatterns #-}
module Checker.Expr where

import Bound
import Bound.Scope
import Control.Applicative
import Control.Monad
import Data.Bifoldable
import Data.Bifunctor
import Data.Bitraversable
import Data.Foldable(Foldable(foldMap))
import Data.List
import qualified Data.Map as M
import Data.Maybe
import Data.Monoid
import qualified Data.Set as S
import Data.String
import Data.Traversable
import Data.Vector(Vector)
import qualified Data.Vector as V
import Prelude.Extras
import Text.PrettyPrint.ANSI.Leijen(align, indent, text, vcat, (<+>), (<$$>), (</>))

import Branches
import Class
import Constr
import Pretty
import qualified Type
import Util
import Util.TopoSort

-- | Expressions with types of type @t@ and free variables of type @v@
data Expr t v
  = Var v
  | Con ECon
  | Lit Literal
  | Let (Expr t v) (Scope1 (Expr t) v)
  | LetRec (Vector (LetScope (Expr t) v)) (LetScope (Expr t) v)
  | Lam (Scope1 (Expr t) v)
  | App (Expr t v) (Expr t v)
  | Case (Expr t v) (Branches (Expr t) v)
  | Anno (Expr t v) t
  | AtNode (Expr t v) Node
  | FatBar (Expr t v) (Expr t v)
  | Fail -- ^ Pattern match failure
  deriving (Eq, Foldable, Functor, Traversable, Show)

instance Eq t => Eq1 (Expr t)
instance Show t => Show1 (Expr t)

type ExprV = Expr Type.TypeC EVar

bindExpr :: (t -> t') -> (v -> Expr t' v') -> Expr t v -> Expr t' v'
bindExpr f g expr = case expr of
  Var v       -> g v
  Con c       -> Con c
  Lit l       -> Lit l
  Let e s     -> Let (bindExpr f g e)
                     (bindExprScope f g s)
  LetRec xs e -> LetRec (bindExprScope f g <$> xs)
                        (bindExprScope f g e)
  Lam s       -> Lam $ bindExprScope f g s
  App e e'    -> bindExpr f g e `App` bindExpr f g e'
  Case e as   -> bindExpr f g e `casee` bindExprBranches f g as
  Anno e t    -> bindExpr f g e `Anno` f t
  AtNode e n  -> bindExpr f g e `AtNode` n
  FatBar e e' -> bindExpr f g e `fatBar` bindExpr f g e'
  Fail        -> Fail

bindExprScope :: (t -> t') -> (v -> Expr t' v')
              -> Scope b (Expr t) v -> Scope b (Expr t') v'
bindExprScope f g (Scope s) = Scope $ bindExpr f (return . fmap (bindExpr f g)) s

opt :: Eq v => Expr t v -> Expr t' v
opt expr = case expr of
  Var v       -> Var v
  Con c       -> Con c
  Lit l       -> Lit l
  Let e s     -> lett (opt e) (optScope s)
  LetRec xs e -> letRec (optScope <$> xs)
                        (optScope e)
  Lam s       -> Lam $ optScope s
  App e e'    -> App (opt e) (opt e')
  Case e as   -> casee' (opt e) (optBranches as)
  Anno e _    -> opt e
  AtNode e n  -> opt e `AtNode` n
  FatBar e e' -> fatBar (opt e) (opt e')
  Fail        -> Fail
  where
    optScope s = toScope . opt $ fromScope s
    optBranches (ConBranches cbrs) = ConBranches
      [ConBranch c n (optScope s) | ConBranch c n s <- cbrs]
    optBranches (LitBranches lbrs def) = LitBranches
      [LitBranch l (opt e) | LitBranch l e <- lbrs] (opt def)

instance Monad (Expr t) where
  return = Var
  e >>= f = bindExpr id f e

instance Applicative (Expr t) where
  pure  = return
  (<*>) = ap

instance Bifunctor Expr where
  bimap = bimapDefault
instance Bifoldable Expr where
  bifoldMap = bifoldMapDefault
instance Bitraversable Expr where
  bitraverse f g expr = case expr of
    Var v       -> Var <$> g v
    Con c       -> pure $ Con c
    Lit l       -> pure $ Lit l
    Let e s     -> Let <$> bitraverse f g e <*> bitraverseScope f g s
    LetRec xs e -> LetRec <$> traverse (bitraverseScope f g) xs
                          <*> bitraverseScope f g e
    Lam s       -> Lam <$> bitraverseScope f g s
    App e e'    -> App <$> bitraverse f g e <*> bitraverse f g e'
    Case e as   -> Case <$> bitraverse f g e <*> bitraverseBranches f g as
    Anno e t    -> Anno <$> bitraverse f g e <*> f t
    AtNode e n  -> AtNode <$> bitraverse f g e <*> pure n
    FatBar e e' -> FatBar <$> bitraverse f g e <*> bitraverse f g e'
    Fail        -> pure Fail

instance HasApp (Expr t v) where
  app = App
  appView (App e e') = pure (e, e')
  appView _          = empty

instance HasAbs (Expr t) where
  abs1 x e = Lam $ abstract1 x e
  absView (Lam s)    = pure s
  absView _          = empty

removeTypeAnnos :: Expr t v -> Expr t' v
removeTypeAnnos expr = case expr of
  Var v       -> Var v
  Con c       -> Con c
  Lit l       -> Lit l
  Let e s     -> Let (removeTypeAnnos e) (removeScope s)
  LetRec xs s -> LetRec (removeScope <$> xs) (removeScope s)
  Lam s       -> Lam $ removeScope s
  App e e'    -> App (removeTypeAnnos e) (removeTypeAnnos e')
  Case e as   -> Case (removeTypeAnnos e) (removeBranches as)
  Anno e _    -> removeTypeAnnos e
  AtNode e n  -> AtNode (removeTypeAnnos e) n
  FatBar e e' -> FatBar (removeTypeAnnos e) (removeTypeAnnos e')
  Fail        -> Fail
  where
    removeScope :: Scope b (Expr t) v -> Scope b (Expr t') v
    removeScope (Scope s) = Scope $ fmap removeTypeAnnos <$> removeTypeAnnos s
    removeBranches (ConBranches cbrs) = ConBranches
      [ConBranch c n $ removeScope s | ConBranch c n s <- cbrs]
    removeBranches (LitBranches lbrs def) = LitBranches
      [LitBranch l $ removeTypeAnnos e | LitBranch l e <- lbrs]
      (removeTypeAnnos def)

-------------------------------------------------------------------------------
-- * Branches
bindExprBranches :: (t -> t') -> (v -> Expr t' v')
           -> Branches (Expr t) v -> Branches (Expr t') v'
bindExprBranches f g (ConBranches brs) = ConBranches
  [ConBranch c n $ bindExprScope f g s | ConBranch c n s <- brs]
bindExprBranches f g (LitBranches brs e) = LitBranches
  [LitBranch l $ bindExpr f g e' | LitBranch l e' <- brs] (bindExpr f g e)

-------------------------------------------------------------------------------
-- * Smart constructors
exposeFails :: Expr t v -> Expr t (Maybe v)
exposeFails expr = case expr of
  Var v       -> Var $ Just v
  Con c       -> Con c
  Lit l       -> Lit l
  Let e s     -> Let (exposeFails e) (exposeScope s)
  LetRec xs s -> LetRec (fmap exposeScope xs) $ exposeScope s
  Lam s       -> Lam $ exposeScope s
  App e e'    -> App (exposeFails e) (exposeFails e')
  Case e brs  -> Case (exposeFails e) $ case brs of
    ConBranches cbrs     -> ConBranches [ ConBranch c n $ exposeScope s
                                        | ConBranch c n s <- cbrs]
    LitBranches lbrs def -> LitBranches [ LitBranch l $ exposeFails e'
                                        | LitBranch l e'   <- lbrs] (exposeFails def)
  Anno e t    -> exposeFails e `Anno` t
  AtNode e n  -> exposeFails e `AtNode` n
  FatBar e e' -> fmap Just e `FatBar` exposeFails e'
  Fail        -> Var Nothing
  where
    exposeScope :: Scope b (Expr t) a -> Scope b (Expr t) (Maybe a)
    exposeScope (Scope s) = Scope $ fmap (fromMaybe (F $ return Nothing)) exposed
      where
        exposed = fmap (fmap exposeFails) <$> exposeFails s

exposeCasedVars :: Expr t v -> Expr t (Either v v)
exposeCasedVars expr = case expr of
  Var v       -> Var $ Right v
  Con c       -> Con c
  Lit l       -> Lit l
  Let e s     -> Let (exposeCasedVars e) (exposeScope s)
  LetRec xs s -> LetRec (fmap exposeScope xs) $ exposeScope s
  Lam s       -> Lam $ exposeScope s
  App e e'    -> App (exposeCasedVars e) (exposeCasedVars e')
  Case e brs  -> Case (caseExpr e) $ case brs of
    ConBranches cbrs     -> ConBranches [ ConBranch c n $ exposeScope s
                                        | ConBranch c n s <- cbrs]
    LitBranches lbrs def -> LitBranches [ LitBranch l $ exposeCasedVars e'
                                        | LitBranch l e' <- lbrs] (exposeCasedVars def)
  Anno e t    -> exposeCasedVars e `Anno` t
  AtNode e n  -> exposeCasedVars e `AtNode` n
  FatBar e e' -> exposeCasedVars e `FatBar` exposeCasedVars e'
  Fail        -> Fail
  where
    caseExpr (Var v) = Var $ Left v
    caseExpr e'      = exposeCasedVars e'
    exposeScope :: Scope b (Expr t) a -> Scope b (Expr t) (Either a a)
    exposeScope (Scope s) = Scope $ fmap (either (fmap Left <$>) id) exposed
      where
        exposed = fmap (fmap exposeCasedVars) <$> exposeCasedVars s

fatBar :: Expr t v -> Expr t v -> Expr t v
fatBar e e' = case numFails of
  0 -> e
  1 -> efails >>= maybe e' return
  _ -> e `FatBar` e'
  where
    efails = exposeFails e
    numFails = length $ filter isNothing $ foldMap (:[]) efails

-- | Sort lets according to dependencies and create a bunch of lets and letrecs
--   accordingly
letRec :: Vector (LetScope (Expr t) v) -> LetScope (Expr t) v -> Expr t v
letRec ss s = instantiate err $ foldr mkLet s groupings
  where
    numberedSs = V.indexed ss
    deps       = M.fromList [(n, S.fromList $ bindings s')
                            | (n, s') <- V.toList numberedSs]

    sortedDeps = topoSort deps
    groupings  = [[numberedSs V.! x | x <- xs]
                 | xs <- sortedDeps]

    mkLet [(n, x)] s'
      | n `S.notMember` S.fromList (bindings x) =
        toScope $ lett (instantiate err $ instantiateSome f $ fmap F x)
                       (instantiateSome f $ fmap F s')
        where
          f n' | n == n'   = B ()
               | otherwise = F (return $ B n')

    mkLet ss' s' =
      toScope $ LetRec (V.fromList $ map (instantiateSome f . fmap F . snd) ss')
                       (instantiateSome f $ fmap F s')
      where
        f n' = maybe (F (return $ B n')) B $ findIndex ((== n') . fst) ss'
    err = error "Checker Expr lett mkLet"

lett :: Expr t v -> Scope1 (Expr t) v -> Expr t v
lett e s = case e of
  Var v     -> instantiate1 (return v) s
  Lit l     -> instantiate1 (Lit l)    s
  Con c     -> instantiate1 (Con c)    s
  (appsView -> (Con c, es)) -> lets es $ toScope $
      Let (apps (Con c) (return <$> vs)) $ toScope $
        exposeCasedVars (fromScope s) >>= f
      where
        vs = take (length es) $ map B [0..]
        f (Left (B ())) = apps (Con c) ((return . F) <$> vs)
        f (Left v)      = return $ F <$> v
        f (Right v)     = return $ F <$> v
  _        -> Let e s

lets :: [Expr t v] -> Scope Int (Expr t) v -> Expr t v
lets []     s = instantiate err s
  where err = error "Checker Expr lets"
lets (e:es) s = lett e $ toScope $ lets (fmap F <$> es) (instantiateSome f $ fmap F s)
  where
    f 0 = F $ return (B ())
    f n = B (n - 1)

casee :: Expr t v -> Branches (Expr t) v -> Expr t v
casee expr brs = case expr of
  (appsView -> (Con c, es)) -> chooseConBranch c es brs
  Lit l                     -> chooseLitBranch l brs
  _                         -> Case expr brs
  where
    chooseConBranch :: ECon -> [Expr t v] -> Branches (Expr t) v -> Expr t v
    chooseConBranch c es (ConBranches cbrs)
      | [s] <- theBranch = lets es s
      where
        len       = length es
        theBranch = [s' | ConBranch c' n s' <- cbrs, c == c', len == n]
    chooseConBranch _ _ _ = error "chooseConBranch"

    chooseLitBranch :: Literal -> Branches f v -> f v
    chooseLitBranch l (LitBranches lbrs def) =
      case [e' | LitBranch l' e' <- lbrs, l == l'] of
        [e'] -> e'
        _    -> def
    chooseLitBranch _ _ = error "chooseLitBranch"

-- | Like casee, but performs case-of-known optimisation
casee' :: Eq v => Expr t v -> Branches (Expr t) v -> Expr t v
casee' expr brs = case expr of
  Var v                     -> Case (Var v) $ propagateVar v brs
  _                         -> casee expr brs
  where
    propagateVar :: Eq v => v -> Branches (Expr t) v -> Branches (Expr t) v
    propagateVar v (ConBranches cbrs) = ConBranches
      [ConBranch c n (go c n $ fromScope s) | ConBranch c n s <- cbrs]
      where
        go c 0 s = toScope $ subst (F v) (Con c) s
        go c n s = toScope $ exposeCasedVars s >>= f
          where
            f (Left (F v')) | v == v' = apps (Con c)
                                             [return $ B b | b <- [0..n - 1]]
            f (Left x)  = return x
            f (Right x) = return x
    propagateVar v (LitBranches lbrs def) = LitBranches
      [LitBranch l $ subst v (Lit l) e | LitBranch l e <- lbrs] def
    subst :: (Monad f, Eq v) => v -> f v -> f v -> f v
    subst v x e = e >>= f
      where
        f v' | v == v'   = x
             | otherwise = return v'

-------------------------------------------------------------------------------
-- Pretty printing
instance (Pretty t, Pretty v, IsString v) => Pretty (Expr t v) where
  prettyPrec ns d expr = case expr of
    Var v       -> prettyPrec ns d v
    Con c       -> prettyPrec ns d c
    Lit l       -> prettyPrec ns d l
    Let e s     -> parensIf (d > letPrec) $
      prettyGenVars ns d 1 $ \ns' _ [defName] def -> align $
        align (text "let" <+> text defName <+> text "=" <+> pretty ns' e) <$$>
        indent 1 (text "in" <+> prettyPrec ns' letPrec (instantiate1 (return $ def 0) s))
    LetRec xs e    -> parensIf (d > letPrec) $
      prettyGenVars ns d (V.length xs) $ \ns' _ defNames def -> align $
        let inst = instantiate (return . def) in
        text "letrec" <+>
        align (vcat [text name <+> text "=" <+> prettyPrec ns' 0 (inst s)
                    | (name, s) <- zip defNames $ V.toList xs]) <$$>
        indent 1 (text "in" <+> prettyPrec ns' letPrec (inst e))
    Lam s       -> prettyAbs ns d "\\" "." (\v -> instantiate1 (return v) s)
    App e e'    -> prettyApp ns d e e'
    Case e brs  -> parensIf (d > casePrec) $
      text "case" <+> prettyPrec ns 0 e <+> text "of" <$$>
        indent 2 (align $ prettyPrec ns d brs)
    Anno e t    -> parensIf (d > annoPrec) $
      prettyPrec ns annoPrec e <+> text ":" <+> prettyPrec ns annoPrec t
    AtNode e n  -> parensIf (d > atNodePrec) $
      prettyPrec ns atNodePrec e <> text "@" <> prettyPrec ns atNodePrec n
    FatBar e e' -> parensIf (d > fatPrec) $
      prettyPrec ns fatPrec e </> text "][" <+> prettyPrec ns (fatPrec + 1) e'
    Fail        -> text "$FAIL"
