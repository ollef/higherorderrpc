module Checker.Prog where

import Control.Applicative
import Data.Map(Map)
import qualified Data.Map as M
import Text.PrettyPrint.ANSI.Leijen(vcat, (<+>), text)

import Constr
import Checker.Expr as Expr
import Pretty
import Type

data Prog = Prog
  { definitions     :: Map EVar ExprV
  , typeDefinitions :: Map TCon (Either TypeDefC DataDefC)
  , constructors    :: Map ECon (TCon, DataDefC)
  } deriving (Show)

opt :: Prog -> Prog
opt (Prog defs ts cs) = Prog (Expr.opt <$> defs) ts cs

-------------------------------------------------------------------------------
-- Pretty printing
instance Pretty Prog where
  pretty ns (Prog defs _ _) = vcat -- TODO print types and datas too?
    [pretty ns n <+> text "=" <+> prettyFree d | (n, d) <- M.toList defs]
