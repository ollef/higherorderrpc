-- | Desugaring of pattern matching
module Checker.Match where

import Bound
import Bound.Var(unvar)
import Control.Applicative
import Data.Bifunctor
import Data.Function
import Data.List(groupBy, nub)
import qualified Data.Map as M
import Data.Maybe

import Branches
import Builtin.Arities(Arity)
import Checker.Expr
import Checker.Prog
import Class
import Constr
import Input.Expr(Pat(..))
import qualified Input.Expr as Input
import qualified Input.Prog as Input
import Type(DataDef(..))
import Util

pass :: Input.Prog -> Prog
pass (Input.Prog defs tdefs constrs) = Prog (fmap go defs) tdefs constrs
  where
    constrsf :: ECon -> [(ECon, Arity)]
    constrsf con = [(c, length ts) | (c, ts) <- cs]
      where
        (_, DataDef _ cs) = fromMaybe unknown $ M.lookup con constrs
        unknown = error $ "Don't know the arity of: " ++ show con
    go :: Input.DefV -> ExprV
    go (Input.Def mn mt cls) = anno (atNode (unvar err id <$> abss bvs e) mn) mt
      where
        -- TODO: Check that patterns are of the same length
        err = error "Match pass go"
        numPats = length . fst . head $ cls
        cls'    = second (fmap F . desugarScope constrsf) <$> cls
        e       = match constrsf (map return bvs) cls' Fail
        bvs     = map B $ take numPats [(0 :: Integer)..]
        atNode e' = maybe e' $ AtNode e'
        anno e'   = maybe e' $ Anno e'

-- | Desugar pattern matching clauses
match :: (ECon -> [(ECon, Arity)])           -- ^ Lookup constructors of a datatype
      -> [Expr t v]                          -- ^ Expressions to case on corresponding to the patterns (usually variables)
      -> [([Pat Int], Scope Int (Expr t) v)] -- ^ The list of patterns
      -> Expr t v                            -- ^ The continuation for pattern match failure
      -> Expr t v
match _ _  []  e0 = e0
match _ [] brs e0 = foldr (fatBar . unScope . snd) e0 brs
  where
    unScope :: Monad f => Scope b f a -> f a
    unScope = instantiate $ error "match unScope"

match constrs xs branches expr0 =
    foldr mix expr0 $ groupBy ((==) `on` patType . firstPat) branches
  where

    patType :: Pat v -> Int
    patType WildP      = 0
    patType (VarP _)   = 0
    patType (ConP _ _) = 1
    patType (LitP _)   = 2

    firstPat = head . fst

    isCon (ConP _ _) = True
    isCon _          = False

    isLit (LitP _)   = True
    isLit _          = False

    mix brs@(f:_) e0 | isCon $ firstPat f = matchCon xs brs e0
                     | isLit $ firstPat f = matchLit xs brs e0
                     | otherwise          = matchVar xs brs e0
    mix _ _ = error "match mix"

    findArity c = fromJust . lookup c $ constrs c

    decon fs = [(unpat p ++ ps, b) | (p:ps, b) <- fs]
      where
        unpat (ConP c pats) | findArity c == length pats = pats
                            | otherwise                  = error $ "wrong arity in pattern match of: " ++ show c
        unpat (LitP _)      = []
        unpat x             = error $ "match unpat: " ++ show x

    matchVar (x:xs') brs e0
      = match constrs xs' (map go brs) e0
      where
        go (VarP y:ps, s) = (ps, subst y x s)
        go (WildP:ps,  s) = (ps, s)
        go _              = error "match matchVar go"
        subst v e s = instantiateSome f s
          where
            f i | i == v    = F e
                | otherwise = B i
    matchVar _ _ _ = error "match matchVar"


    matchLit (x:xs') brs e0 =
      let ls = nub $ map (lit . firstPat) brs
          lbrs = do
            l <- ls
            let brs' = filter ((== LitP l) . firstPat) brs
            return $ LitBranch l $ match constrs xs' (decon brs') Fail
       in casee x (LitBranches lbrs e0)
      where
        lit (LitP l) = l
        lit p        = error $ "match lit: " ++ show p
    matchLit _ _ _ = error "match matchLit"

    matchCon (x:xs') brs e0 =
      let cbrs = do
            (c, arity) <- constrs (firstCon $ head brs)
            let brs' = [second (fmap F) br | br <- brs, firstCon br == c]
                ys   = map (return . B) $ take arity [0..]
                xs'' = map (fmap F) xs'
            return $ ConBranch c arity
                   $ toScope
                   $ match constrs (ys ++ xs'') (decon brs') Fail
       in casee x (ConBranches cbrs) `fatBar` e0
      where
        firstCon = constr . firstPat
        constr (ConP c _) = c
        constr _          = error "match constr"
    matchCon _ _ _ = error "match matchCon"

desugarScope :: (Eq b, Eq t, Eq v)
             => (ECon -> [(ECon, Arity)])
             -> Scope b (Input.Expr t) v
             -> Scope b (Expr       t) v
desugarScope p (Scope s) = Scope $ desugar p $ fmap (desugar p) <$> s

desugar :: (Eq t, Eq v)
        => (ECon -> [(ECon, Arity)])
        -> Input.Expr t v
        -> Expr t v
desugar p iexpr = case iexpr of
  Input.Var v      -> Var v
  Input.Con c      -> Con c
  Input.Lit l      -> Lit l
  Input.Let cls s  -> letRec (fmap (uncurry3 desugarClauses) cls) $ desugarScope p s
  Input.Lam s      -> Lam $ desugarScope p s
  Input.App e e'   -> desugar p e `app` desugar p e'
  Input.Case e brs ->
    lett (desugar p e) $ toScope
      $ match p [return $ B ()] [([pat], F <$> desugarScope p s)
                                | Input.Branch pat s <- brs]
              Fail
  Input.Anno e t   -> desugar p e `Anno` t
  Input.AtNode e n -> desugar p e `AtNode` n
  where
    uncurry3 f (x, y, z) = f x y z

    desugarClauses :: (Eq t, Eq v) => Maybe Node -> Maybe t -> Input.Clauses t v -> LetScope (Expr t) v
    desugarClauses mn mt cl = toScope $ anno (fmap (unvar err id) <$> abss absvs cases)
      where                       -- pat               def      abs
        -- cl' :: [([Pat Int], Scope Int (Expr t) (Var Int (Var Int v)))]
        dcl     = [(ps, desugarScope p s) | (ps, s) <- cl]

        inst :: Monad f => Either a b -> Var a (f (Var b (Var v v')))
        inst = either B (F . return . B)

        dcl'    = [(ps, instantiateSome inst $ fmap (F . F) s) | (ps, s) <- dcl]

        numpats = length $ fst $ head cl

        absvs   = (F . B) <$> take numpats [(0 :: Int)..]
        --                      def      abs
        -- cases :: Expr t (Var Int (Var Int v))
        cases   = match p (map return absvs) dcl' Fail
        err = error "desugar desugarClauses"
        anno = maybe id (flip Anno)   mt
             . maybe id (flip AtNode) mn
