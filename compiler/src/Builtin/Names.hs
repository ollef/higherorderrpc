module Builtin.Names where

import Prelude hiding (and, or, mod, div)

import Constr (EVar(..), ECon(..), TCon(..))

arrow :: TCon
arrow = TCon "->"

-- fail :: EVar
-- fail = EVar "$fail"

true, false :: ECon
true  = ECon "True"
false = ECon "False"

cons, nil :: ECon
cons  = ECon "_Cons"  -- h :: t
nil   = ECon "_Nil"   -- []

maxTuple :: Int
maxTuple = 32

tuple :: Int -> ECon
tuple n | n >= 0 && n <= maxTuple = ECon $ "_Tuple" ++ show n -- (a, b, c, ...)
        | otherwise               = error $ "Tuples can not have arity " ++ show n

add, sub, mul, div, mod, shl, shr, and, or, eq, neq, lt, gt, lteq, gteq, compose, append, seq, printInt, printChar, terminate
  :: EVar
add       = EVar "_add"     -- x + y
sub       = EVar "_sub"     -- x - y
mul       = EVar "_mul"     -- x * y
div       = EVar "_div"     -- x / y
mod       = EVar "_mod"     -- x % y
shl       = EVar "_shl"     -- x << y
shr       = EVar "_shr"     -- x >> y
and       = EVar "_and"     -- x && y
or        = EVar "_or"      -- x || y
eq        = EVar "_eq"      -- x == y
neq       = EVar "_neq"     -- x /= y
lt        = EVar "_lt"      -- x < y
gt        = EVar "_gt"      -- x > y
lteq      = EVar "_lteq"    -- x <= y
gteq      = EVar "_gteq"    -- x >= y
compose   = EVar "compose"  -- g . f
append    = EVar "append"   -- xs ++ ys
seq       = EVar "seq"      -- f ; g
printInt  = EVar "print_int"
printChar = EVar "print_char"
terminate = EVar "terminate"

-- | Built-in user-facing functions that are defined in the runtime system
primitiveFunctions :: [EVar]
primitiveFunctions =
  [add, sub, mul, div, mod, eq, neq, lt, gt, lteq, gteq
  ,printInt, printChar, terminate
  ]

listT, boolT
  :: TCon
listT = TCon "_List"
boolT = TCon "Bool"

tupleT :: Int -> TCon
tupleT n = let ECon s = tuple n in TCon s

-- * Runtime system functions
apply :: Int -> EVar
apply n = EVar $ "Apply_" ++ show n

pap :: Int -> Int -> EVar
pap m n = EVar $ "Pap_" ++ show m ++ "_" ++ show n

tagFunPtr, untagFunPtr, tagRemoteFunPtr, untagRemoteFunPtr, tagRemoteFunId, untagRemoteFunId, remoteApplySend, remoteApplyReceive, functionTable, lookupFunction, isLocalNode, mobileCutoff :: EVar
tagFunPtr               = EVar "TagFunctionPointer"
untagFunPtr             = EVar "UntagFunctionPointer"
tagRemoteFunPtr         = EVar "TagRemoteFunctionPointer"
untagRemoteFunPtr       = EVar "UntagRemoteFunctionPointer"
tagRemoteFunId          = EVar "TagRemoteFunctionId"
untagRemoteFunId        = EVar "UntagRemoteFunctionId"
remoteApplySend         = EVar "RemoteApplySend"
remoteApplyReceive      = EVar "RemoteApplyReceive"
functionTable           = EVar "FunctionTable"
lookupFunction          = EVar "LookupFunction"
isLocalNode             = EVar "IsLocalNode"
mobileCutoff            = EVar "MobileCutoff"

remoteApply :: Int -> EVar
remoteApply n = EVar $ "RemoteApply_" ++ show n
nodeOf, idOf :: EVar -> EVar
nodeOf (EVar s) = EVar $ "Node_" ++ s
idOf   (EVar s) = EVar $ "Id_" ++ s
