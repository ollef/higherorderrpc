module Builtin.Types where

import Bound
import Control.Applicative

import Constr
import Class
import Type
import qualified Builtin.Names as Builtin

defineData :: Int                             -- ^ Number of type params
           -> [(ECon, [Type (Var Int TCon)])] -- ^ Constructors
           -> DataDefC
defineData n cs = DataDef n [(c, map toScope ts) | (c, ts) <- cs]

list :: DataDefC
list = defineData 1
  [ (Builtin.nil,  [])
  , (Builtin.cons, [a, app (Var $ F Builtin.listT) a])
  ] where a = Var $ B 0

tuple :: Int -> DataDefC
tuple n = defineData n [(Builtin.tuple n, (Var . B) <$> take n [0..])]

bool :: DataDefC
bool = defineData 0 [(Builtin.false, []), (Builtin.true, [])]

dataDefinitions :: [(TCon, DataDefC)]
dataDefinitions =
  [ (Builtin.listT, list)
  , (Builtin.boolT, bool)
  ] ++
  [(Builtin.tupleT n, tuple n) | n <- [0..Builtin.maxTuple]]
