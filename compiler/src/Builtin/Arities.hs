module Builtin.Arities where

import qualified Data.Map as M
import qualified Builtin.Names as Builtin
import Constr

type Arity = Int

findVarArity :: EVar -> Maybe Arity
findVarArity var = M.lookup var varArities

varArities :: M.Map EVar Arity
varArities = M.fromList
  [ (Builtin.add,       2)
  , (Builtin.sub,       2)
  , (Builtin.mul,       2)
  , (Builtin.div,       2)
  , (Builtin.mod,       2)
  , (Builtin.shl,       2)
  , (Builtin.shr,       2)
  , (Builtin.and,       2)
  , (Builtin.or,        2)
  , (Builtin.eq,        2)
  , (Builtin.neq,       2)
  , (Builtin.lt,        2)
  , (Builtin.gt,        2)
  , (Builtin.lteq,      2)
  , (Builtin.gteq,      2)
  , (Builtin.compose,   2)
  , (Builtin.append,    2)
  , (Builtin.seq,       2)
  , (Builtin.printInt,  2)
  , (Builtin.printChar, 2)
  , (Builtin.terminate, 1)
  ]
