module Builtin.Functions where

import qualified Builtin.Names as Builtin
import Constr (EVar(..), ECon(..))
import Input.Expr (ExprV, Expr(..), iff)

binCon :: ECon -> ExprV -> ExprV -> ExprV
binCon con x = App (App (Con con) x)

binVar :: EVar -> ExprV -> ExprV -> ExprV
binVar var x = App (App (Var var) x)

cons, add, sub, mul, div, mod, shl, shr, and, or, eq, neq, lt, gt, lteq, gteq, compose, append, seq
  :: ExprV -> ExprV -> ExprV
cons    = binCon Builtin.cons    -- h :: t
add     = binVar Builtin.add     -- x + y
sub     = binVar Builtin.sub     -- x - y
mul     = binVar Builtin.mul     -- x * y
div     = binVar Builtin.div     -- x / y
mod     = binVar Builtin.mod     -- x % y
shl     = binVar Builtin.shl     -- x << y
shr     = binVar Builtin.shr     -- x >> y
eq      = binVar Builtin.eq      -- x == y
neq     = binVar Builtin.neq     -- x /= y
lt      = binVar Builtin.lt      -- x < y
gt      = binVar Builtin.gt      -- x > y
lteq    = binVar Builtin.lteq    -- x <= y
gteq    = binVar Builtin.gteq    -- x >= y
compose = binVar Builtin.compose -- g . f
append  = binVar Builtin.append  -- xs ++ ys
seq     = binVar Builtin.seq     -- f ; g

-- x && y
and x y = iff x y false
  where false = Con Builtin.false

-- x || y
or x y  = iff x true y
  where true = Con Builtin.true
