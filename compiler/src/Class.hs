{-# LANGUAGE ViewPatterns #-}
module Class where

import Bound
import Bound.Var
import Control.Arrow(first)
import Control.Applicative

import Util

infixl 4 `app`
infixl 4 `apps`
infixr 4 -->

class HasArrow a where
  (-->)     :: a -> a -> a
  x --> y = [x] `arrows` y

  arrows    :: [a] -> a -> a
  arrows = flip $ foldr (-->)

  arrowView :: Alternative r => a -> r (a, a)
  arrowView x = case arrowsView x of
    (a:as, b) -> pure (a, as `arrows` b)
    _         -> empty

  arrowsView :: a -> ([a], a)
  arrowsView x = case arrowView x of
    Just (a, b) -> first (a:) $ arrowsView b
    Nothing     -> ([], x)

  {-# MINIMAL ((-->) | arrows), (arrowView | arrowsView) #-}

class HasApp a where
  app  :: a -> a -> a
  app x y = apps x [y]

  apps :: a -> [a] -> a
  apps = foldl app

  appView  :: Alternative r => a -> r (a, a)
  appView a = case reverse xs of
      x':_ -> pure (x `apps` init xs, x')
      _    -> empty
    where
      (x, xs) = appsView a

  appsView :: a -> (a, [a])
  appsView = go []
    where
      go xs (appView -> Just (x, x')) = go (x':xs) x
      go xs x                         = (x, xs)

  {-# MINIMAL (app | apps), (appView | appsView) #-}

class (Functor f, Monad f) => HasAbs f where
  abs1 :: Eq v => v -> f v -> f v
  abs1 x = abss [x]

  abss :: Eq v => [v] -> f v -> f v
  abss = flip $ foldr abs1

  absView :: Alternative r => f v -> r (Scope1 f v)

  abssView :: Alternative r => f v -> r (Int, Scope Int f v)
  abssView = go 0 . fmap F
    where
      go n (absView -> Just s) = go (n + 1) (instantiate1 (return $ B n) s)
      go 0 _ = empty
      go n x = pure (n, fmap (unvar err id) $ abstract f x)
        where
          err = error "Util abssView"
          f (B n') = Just n'
          f _      = Nothing

  {-# MINIMAL (abs1 | abss), absView  #-}
