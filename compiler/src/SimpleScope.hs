{-# LANGUAGE DeriveFoldable, DeriveFunctor, DeriveTraversable, FlexibleInstances, MultiParamTypeClasses #-}
-- | We can't use Bound.Scope for things that are not Monads, so here's a Scope
--   type for such things.
module SimpleScope where

import Bound.Var
import Data.Bifunctor
import Data.Foldable
import Data.Functor
import Data.Traversable
import Prelude.Extras

newtype Scope b f a = Scope {unScope :: f (Var b a)}
  deriving (Functor, Foldable, Traversable)
type Scope1 = Scope ()

instantiate :: Functor f => (b -> a) -> Scope b f a -> f a
instantiate f (Scope s) = unvar f id <$> s

mapScope :: (f (Var b a) -> f' (Var b' a')) -> Scope b f a -> Scope b' f' a'
mapScope f (Scope x) = Scope $ f x

instantiate1 :: Functor f => a -> Scope b f a -> f a
instantiate1 x s = instantiate (const x) s

mapVars :: Functor f => (b -> b') -> (a -> a') -> Scope b f a -> Scope b' f a'
mapVars f g (Scope x) = Scope $ bimap f g <$> x

mapBound :: Functor f => (b -> b') -> Scope b f a -> Scope b' f a
mapBound f = mapVars f id

abstract :: Functor f => (a -> Maybe b) -> f a -> Scope b f a
abstract f x = Scope $ fmap g x
  where
    g a = maybe (F a) B $ f a

abstract1 :: (Eq a, Functor f) => a -> f a -> Scope1 f a
abstract1 a x = abstract f x
  where
    f a' | a == a'   = Just ()
         | otherwise = Nothing

instance (Show b, Show1 f, Show a) => Show (Scope b f a) where
  showsPrec = showsPrec1
instance (Show b, Show1 f) => Show1 (Scope b f) where
  showsPrec1 d (Scope s) = showParen (d > 10) $
      showString "Scope " . showsPrec1 11 s

instance (Eq b, Eq1 f, Eq a) => Eq (Scope b f a) where
  (==) = (==#)
instance (Eq b, Eq1 f) => Eq1 (Scope b f) where
  Scope a ==# Scope b = a ==# b

instance (Ord b, Ord1 f, Ord a) => Ord  (Scope b f a) where
  compare = compare1
instance (Ord b, Ord1 f) => Ord1 (Scope b f) where
  compare1 (Scope a) (Scope b) = compare1 a b
