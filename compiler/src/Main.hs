module Main where

import Control.Monad hiding (forM_)
import Data.Foldable(forM_)
import Data.List(intercalate)
import Data.Maybe
import Data.Text.Lazy(Text)
import qualified Data.Text.Lazy as T
import qualified Data.Text.Lazy.IO as T
import System.Console.GetOpt
import System.Directory
import System.Environment
import System.Exit
import System.FilePath
import System.Process

import Paths_myrkotte

import Constr
import qualified Backend.Closure
import qualified Backend.CodeGen
import qualified Backend.Restrict
import qualified Checker.Match
import qualified Checker.Prog
import qualified Parser
import Pretty

runtimeOutdir :: FilePath
runtimeOutdir = "runtime"

passes :: Bool -> FilePath -> FilePath -> IO [FilePath]
passes v outdir = Parser.pass         >=> prettier "parsed"
       >=> purr Checker.Match.pass    >=> prettier "desugared pattern-matching"
       >=> purr Checker.Prog.opt      >=> prettier "opt"
       >=> purr Backend.Restrict.pass >=> prettier "first-order"
       >=> purr Backend.Closure.pass  >=> prettier "closure-converted"
       >=> generateFiles
  where
    generateFiles p = do
      let nodes         = Backend.CodeGen.pass p
          closureFile   = Backend.CodeGen.closureFile p
          runtimeDir    = "c-runtime"
          runtimeFiles  = ["runtime.h", "runtime.c", "value.h", "value.c", "memory.h", "memory.c", "serial.c", "serial.h"]
      createDirectoryIfMissing True outdir
      createDirectoryIfMissing True (outdir </> runtimeOutdir)
      files <- forM nodes $ \(node, contents) -> do
        let nodeFile = unNode node <.> "c"
        T.writeFile (outdir </> nodeFile) contents
        return nodeFile
      T.writeFile (outdir </> runtimeOutdir </> "closure.h") closureFile
      forM_ runtimeFiles $ \f -> do
        filename <- getDataFileName (runtimeDir </> f)
        copyFile filename (outdir </> runtimeOutdir </> f)
      return files

    purr p = return . p

    prettier name x = do
      when v $ do
        printLine
        putStrLn $ "-- * " ++ name
        putPretty x
        putStrLn ""
      return x
{-
    text name x = do
      when v $ do
        printLine
        putStrLn $ "-- * " ++ name
        T.putStr x
        putStrLn ""
      return x

    shower name x = do
      when v $ do
        printLine
        putStrLn $ "-- * " ++ name
        print x
      return x
-}

runScript :: Maybe String -> [FilePath] -> Text
runScript mdebugger files = T.unlines
  [T.pack "#!/bin/sh"
  ,T.pack $ "mpirun" ++ intercalate " :" (map (f mdebugger) files)
  ]
  where
    f (Just debugger) file = " -np 1 " ++ debugger ++ " " ++ file
    f Nothing         file = " -np 1 " ++ file

main :: IO ()
main = do
  args <- getArgs
  let (actions, nonOptions, errors) = getOpt RequireOrder options args
  printList "Non-options" nonOptions
  printList "Errors" errors
  opts <- foldl (>>=) (return defaultOptions) actions
  let Options { optInputFile = inputFile
              , optOutputDir = outputDir
              , optVerbose   = verbose
              , optDebugger  = debugger
              , optJustC     = justC
              , optExeName   = exeName
              , optCFlags    = cflags
              } = opts
      binDir = "bin"
  createDirectoryIfMissing True (outputDir </> binDir)
  forM_ inputFile $ \file -> do
    generatedFiles <- passes verbose outputDir file
    unless justC . void $ do
      let exeName' = fromMaybe (outputDir </> "main") exeName
      T.writeFile exeName'
        $ runScript debugger [outputDir </> binDir </> dropExtension f | f <- generatedFiles]
      _ <- system $ "chmod +x " ++ exeName'
      gccflags <- readProcess "pkg-config" ["--cflags", "bdw-gc"] ""
      gclibs   <- readProcess "pkg-config" ["--libs", "bdw-gc"] ""
      forM_ generatedFiles $ \generatedFile -> -- We want to compile each file as a separate binary
        system . intercalate " " $ concat
          [ ["mpicc"]
          , map (outputDir </>)
            [ generatedFile ]
          , map ((outputDir </> runtimeOutdir) </>)
            [ "runtime.c"
            , "value.c"
            , "memory.c"
            , "serial.c"
            ]
          , map (convOpt Single Space)
            [ ("o", outputDir </> binDir </> dropExtension generatedFile)
            , ("I", outputDir)
            ]
          , map (convOpt Single Equals)
            [ ("falign-functions", "8")
            , ("std", "c99")
            ]
          , [ "-pedantic"
            , "-Wall"
            , "-Wno-unused-variable"
            , "-Wno-return-type"
            , "-O2"
            ]
          , if isJust debugger then ["-g"] else []
          , lines gccflags
          , lines gclibs
          , cflags
          ]
  return ()

printList :: String -> [String] -> IO ()
printList title list = unless (null list) $ do
  putStrLn $ title ++ ":"
  forM_ list $ putStrLn . (" - "++)
  printLine

printLine :: IO ()
printLine = putStrLn $ replicate 80 '-'

data Options = Options
  { optInputFile :: Maybe FilePath
  , optOutputDir :: FilePath
  , optVerbose   :: Bool
  , optDebugger  :: Maybe String
  , optJustC     :: Bool
  , optExeName   :: Maybe String
  , optCFlags    :: [String]
  }

defaultOptions :: Options
defaultOptions = Options
  { optInputFile = Nothing
  , optOutputDir = "out"
  , optVerbose   = False
  , optDebugger  = Nothing
  , optJustC     = False
  , optExeName   = Nothing
  , optCFlags    = []
  }

options :: [OptDescr (Options -> IO Options)]
options =
  [ Option "i" ["input"]
      (ReqArg
        (\file opts -> do
          exists <- doesFileExist file
          if exists
          then return opts { optInputFile = Just file }
          else putStrLn ("File " ++ file ++ " does not exist.") >> exitFailure)
        "FILE")
      "Input file"
  , Option "d" ["outdir"]
      (ReqArg
        (\dir opts -> return opts { optOutputDir = dir })
        "DIR")
      "Output directory"
  , Option "v" ["verbose"]
      (NoArg $ \opts -> return opts { optVerbose = True })
      "Be verbose"
  , Option "c" ["just-c"]
      (NoArg $ \opts -> return opts { optJustC = True })
      "Just produce the C files, do not create any executables"
  , Option "" ["c-flag"]
    (ReqArg
        (\flag opts -> return opts {optCFlags = optCFlags opts ++ [flag]})
        "FLAG")
      "Pass this flag to the C compiler"
  , Option "g" ["debugger"]
      (ReqArg
        (\debugger opts -> return opts {optDebugger = Just debugger})
        "NAME")
      "Enable debug mode"
  , Option "o" ["out", "output"]
      (ReqArg
        (\name opts -> return opts { optExeName = Just name })
        "FILE")
      "Specify the name of the executable script"
  , Option "h" ["help", "usage"]
      (NoArg . const $ do
        prog <- getProgName
        putStrLn $ usageInfo prog options
        exitSuccess)
      "Show help"
  ]


data Hyphenation = Single | Double
data Delimiter   = Space  | Equals

convOpt :: Hyphenation -> Delimiter -> (String, String) -> String
convOpt Single Space  (k, v) = "-"  ++ k ++ " " ++ v
convOpt Single Equals (k, v) = "-"  ++ k ++ "=" ++ v
convOpt Double Space  (k, v) = "--" ++ k ++ " " ++ v
convOpt Double Equals (k, v) = "--" ++ k ++ "=" ++ v
