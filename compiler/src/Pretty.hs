module Pretty where

import Bound.Var
import Data.Foldable(Foldable(..))
import Data.Monoid
import qualified Data.Set as S
import Data.String
import qualified Data.Vector as V
import Text.PrettyPrint.ANSI.Leijen(Doc, renderPretty, displayIO, tupled, parens, list, text, hsep, (<+>))
import System.IO(stdout)

names :: [String]
names = do
  n <- [(0 :: Int)..]
  c <- ['a'..'z']
  return $ c : if n == 0 then "" else show n

purdy :: Pretty a => a -> Doc
purdy = pretty names

putPretty :: Pretty a => a -> IO ()
putPretty x = displayIO stdout $ renderPretty 1 100 $ purdy x

prettyFree :: (Ord v, IsString v, Foldable f, Pretty (f v)) => f v -> Doc
prettyFree x = pretty (filter ((`S.notMember` fv) . fromString) names) x
  where
    fv = foldMap S.singleton x

putPrettyFree :: (Ord v, IsString v, Foldable f, Pretty (f v)) => f v -> IO ()
putPrettyFree x = displayIO stdout $ renderPretty 1 100 $ prettyFree x

appPrec, absPrec, arrPrec, fatPrec, annoPrec, casePrec, letPrec, atNodePrec :: Int
appPrec    = 11
absPrec    =  1
arrPrec    =  1
fatPrec    =  2
annoPrec   =  0
atNodePrec =  12
casePrec   =  1
letPrec    =  1

type PrettyFun a = [String] -> Int -> a

class Pretty a where
  prettyPrec :: PrettyFun (a -> Doc)
  prettyPrec ns _ = pretty ns

  pretty :: [String] -> a -> Doc
  pretty ns = prettyPrec ns 0

instance Pretty Doc where
  pretty _ = id

instance Pretty () where
  pretty _ _ = tupled []

instance (Pretty a, Pretty b) => Pretty (a, b) where
  pretty ns (a, b) = tupled [pretty ns a, pretty ns b]

instance (Pretty a, Pretty b, Pretty c) => Pretty (a, b, c) where
  pretty ns (a, b, c) = tupled [pretty ns a, pretty ns b, pretty ns c]

instance Pretty a => Pretty [a] where
  pretty ns xs = list $ map (pretty ns) xs

instance Pretty a => Pretty (Maybe a) where
  prettyPrec _  _ Nothing  = text "Nothing"
  prettyPrec ns d (Just x) = prettyApp ns d (text "Just") x

instance (Pretty a, Pretty b) => Pretty (Var a b) where
  prettyPrec ns d (B b) = prettyApp ns d (text "B") b
  prettyPrec ns d (F f) = prettyApp ns d (text "F") f

instance Pretty Int where pretty _ = text . show
instance Pretty Integer where pretty _ = text . show

parensIf :: Bool -> Doc -> Doc
parensIf True  = parens
parensIf False = id

parensPretty :: Pretty a => PrettyFun (Bool -> a -> Doc)
parensPretty ns _ True  x = parens $ pretty ns x
parensPretty ns d False x = prettyPrec ns d x

infixr 6 <++>
(<++>) :: String -> Doc -> Doc
"" <++> d = d
s  <++> d = text s <+> d

prettyAbs :: (IsString v, Pretty e) => PrettyFun (String -> String -> (v -> e) -> Doc)
prettyAbs (v:vs) d lam dot f = parensIf (d > absPrec) $
  lam <++> text v <> dot <++> prettyPrec vs absPrec (f $ fromString v)
prettyAbs [] _ _ _ _ = error "prettyAbs"

prettyGenVars :: IsString v => PrettyFun (Int -> PrettyFun ([String] -> (Int -> v) -> a) -> a)
prettyGenVars vs d numVars f = f fvs d bvs lu
  where
    (bvs, fvs) = splitAt numVars vs
    vecbvs = V.fromList $ map fromString bvs
    lu i = vecbvs V.! i

prettyAbss :: IsString v =>
  PrettyFun (String -> String -> Int -> PrettyFun ((Int -> v) -> Doc) -> Doc)
prettyAbss vs d lam dot numVars f =
  parensIf (d > absPrec) $ prettyGenVars vs absPrec numVars $ \fvs _ bvs lu ->
  lam <++> hsep (map text bvs) <+> dot <++> f fvs absPrec lu

prettyApp :: (Pretty a, Pretty b) => PrettyFun (a -> b -> Doc)
prettyApp ns d a b = parensIf (d > appPrec) $
  prettyPrec ns appPrec a <+> prettyPrec ns (appPrec + 1) b

prettyApps :: PrettyFun (PrettyFun Doc -> [PrettyFun Doc] -> Doc)
prettyApps ns d a bs = parensIf (d > appPrec) $
  a ns appPrec <+> hsep (map (\b -> b ns (appPrec + 1)) bs)

prettyPrecF :: Pretty a => a -> PrettyFun Doc
prettyPrecF a ns d = prettyPrec ns d a
