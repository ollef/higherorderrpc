let compose g f x = g (f x);;

let rec append xs ys = match xs with
  | []     -> ys
  | (h::t) -> h :: append t ys;;

let rec range start endd =
  if start <= endd
  then start :: range (start + 1) endd
  else [];;

let rec foldr f acc = function
  | []     -> acc
  | (h::t) -> f h (foldr f acc t);;

let rec map f = function
  | []     -> []
  | (h::t) -> f h :: map f t;;

let concat = foldr append [];;

let concatMap f = compose concat (map f);;

let reverse =
  let rec reverse_acc acc = function
    | h::t -> reverse_acc (h :: acc) t
    | _    -> acc
  in
  reverse_acc [];;

let rec filter p = function
  | []     -> []
  | (h::t) -> if p h
              then h :: filter p t
              else filter p t;;

let rec elem x = function
  | []     -> false
  | (h::t) -> x == h || elem x t;;

let rec enumerate n = function
  | []     -> []
  | (h::t) -> (n, h) :: enumerate (n + 1) t;;

let any p = foldr (fun x acc -> acc || p x) false;;

let absDiff x y =
  let n = x - y in
  if n < 0 then -n else n;;

let safe q qs =
  let sameDiag =
    any (fun (dist, q') -> absDiff q q' == dist)
        (enumerate 1 qs)
  in
  not (elem q qs || sameDiag);;

let queensSlow n =
  let rec gen = function
    | 0 -> [[]]
    | k -> concatMap (fun q -> concatMap (fun qs -> [(q :: qs)])
                                         (gen (k - 1)))
                     (range 1 n)
  in
  let rec test = function
    | []       -> true
    | (h :: t) -> safe h t && test t
  in
  filter test (gen n);;

let queensFast n =
  let rec gen = function
    | 0 -> [[]]
    | k -> concatMap (fun qs -> concatMap (fun q -> (if safe q qs
                                                     then [(q :: qs)]
                                                     else []))
                                          (range 1 n))
                     (gen (k - 1))
  in
  map reverse (gen n);;

let rec print_board = function
  | []           -> ()
  | ([]::t)      -> print_char '\n' 
  | ((x::xs)::t) -> print_int x ; print_char '\t' ;;

let main () = print_string "Generating...\n"
            ; print_board (queensFast 8)
            ; print_string "Done\n";;

main ();;

