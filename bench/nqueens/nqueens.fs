absDiff x y = let n = x - y in
              if n < 0 then 0 - n else n

safe : Int -> [Int] -> Bool
  q qs = not (elem q qs || sameDiag)
    where
      sameDiag : Bool
        = any (\x. case x of
                (dist, q') -> absDiff q q' == dist)
              (enumerate 1 qs)

queensSlow : Int -> [[Int]]
  n = filter test (gen n)
    where
      gen : Int -> [[Int]]
        0 = [[]]
        k = concatMap (\q. concatMap (\qs. [q :: qs])
                                     (gen (k - 1)))
                      (range 1 n)
      test : [Int] -> Bool
        []        = True
        (h :: t)  = safe h t && test t

queensFast : Int -> [[Int]]
  n = map reverse (gen n)
    where
    gen : Int -> [[Int]]
      0 = [[]]
      k = concatMap (\qs. concatMap (\q. (if safe q qs
                                          then [q :: qs]
                                          else []))
                                    (range 1 n))
                    (gen (k - 1))

main@M = (print_board (queensFast 8); terminate) ()

print_board : [[Int]] -> IO ()
  []           = print_char '-' ; \x. ()
  ([]::t)      = print_char '\n' 
  ((x::xs)::t) = print_int x ; print_char '\t' 

compose : (b -> c) -> (a -> b) -> a -> c
  g f x = g (f x)

not : Bool -> Bool
  False = True
  True  = False

append : [a] -> [a] -> [a]
  []     ys = ys
  (h::t) ys = h :: append t ys

type IO a = () -> a

seq : IO () -> IO a -> IO a
  a b x = b (a x)

range : Int -> Int -> [Int]
  start end = if start <= end
              then start :: range (start + 1) end
              else []

concat : [[a]] -> [a]
  = foldr append []

concatMap : (a -> [b]) -> [a] -> [b]
  f = concat . map f

map : (a -> b) -> [a] -> [b]
  f []     = []
  f (h::t) = f h :: map f t

reverse : [a] -> [a]
  = reverse_acc []
    where
      reverse_acc : [a] -> [a] -> [a]
        acc xs = case xs of
          h::t -> reverse_acc (h :: acc) t
          _    -> acc

filter : (a -> Bool) -> [a] -> [a]
  p []     = []
  p (h::t) = if p h
             then h :: filter p t
             else filter p t

foldr : (a -> b -> b) -> b -> [a] -> b
  f acc []     = acc
  f acc (h::t) = f h (foldr f acc t)

elem : a -> [a] -> Bool
  x []     = False
  x (h::t) = x == h || elem x t

enumerate : Int -> [a] -> [(Int, a)]
  n []     = []
  n (h::t) = (n, h) :: enumerate (n + 1) t

any : (a -> Bool) -> [a] -> Bool
  p = foldr (\x acc. acc || p x) False

