import os
from time import perf_counter

class Result(object):
    def __init__(self, name, singleTime, multiTime, ocamlTime):
        self.name = name
        self.singleTime = singleTime
        self.multiTime = multiTime
        self.ocamlTime = ocamlTime

def time(name, file, runs):
    # Build the examples
    os.system('myrkotte -i ' + file + '.fs -d ' + file + '_single_files -o ' + file + '.fs_run')
    os.system('myrkotte -i ' + file + '2.fs -d ' + file + '_multi_files -o ' + file + '2.fs_run')
    # Run (and time!) them
    singleTime = 0.0
    multiTime  = 0.0
    ocamlTime  = 0.0
    for i in range(0, runs):
        singleTime += getTime(file + '.fs_run')
        multiTime  += getTime(file + '2.fs_run')

    # Clean up
    os.system('rm ' + file + '.fs_run ' + file + '2.fs_run')
    os.system('rm -rf ' + file + '_single_files ' + file + '_multi_files')

    return Result(name, singleTime, multiTime, ocamlTime)

def getTime(s):
    start = perf_counter()
    os.system(s)
    end = perf_counter()
    diff = end - start
    return diff

results = [
    time('Naive generation of primes', 'naive-primes/primes', 5),
    time('The "tak" function', 'tak/tak', 5),
    time('Naive fibonacci generation', 'naive-fib/fib', 5),
    time('Quicksort', 'quicksort/quicksort', 5),
    time('Binary tree (de)constructions', 'trees/trees', 5),
    time('Solving the n-queens problem', 'nqueens/nqueens', 5)
]

text = ""
for result in results:
    text += result.name + ':\n'
    text += '  Single-node: ' + ('%.3g' % result.singleTime) + '\n'
    text += '  Multi-node:  ' + ('%.3f' % result.multiTime)  + '\n'
    text += '  Delta        ' + ('%.3g' % (result.multiTime - result.singleTime)) + '\n'

# Write file
with open('benchmarks.txt', 'w') as f:
    f.write(text)
