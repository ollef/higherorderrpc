import os
from time import perf_counter

class Result(object):
    def __init__(self, name, singleTime, multiTime, ocamlTime):
        self.name = name
        self.singleTime = singleTime
        self.multiTime = multiTime
        self.ocamlTime = ocamlTime

def time(name, file, runs):
    # Build the examples
    os.system('myrkotte -i ' + file + '.fs -d ' + file + '_single_files -o ' + file + '.fs_run')
    os.system('ocamlopt ' + file  + '.ml' + ' -o ' + file + '.ocaml_run')

    # Run (and time!) them
    singleTime = 0.0
    multiTime  = 0.0
    ocamlTime  = 0.0
    for i in range(0, runs):
        singleTime += getTime(file + '.fs_run')
        ocamlTime  += getTime(file + '.ocaml_run')

    # Clean up
    os.system('rm ' + file + '.fs_run ' + file + '.ocaml_run')
    os.system('rm -rf ' + file + '_single_files ')
    os.system('rm ' + file + '.cmi ' + file + '.cmx')

    return Result(name, singleTime, multiTime, ocamlTime)

def getTime(s):
    start = perf_counter()
    os.system(s)
    end = perf_counter()
    diff = end - start
    return diff # '%0.3f' % diff

results = [
    time('Naive generation of primes', 'naive-primes/primes', 100),
    time('The "tak" function', 'tak/tak', 50),
    time('Naive fibonacci generation', 'naive-fib/fib', 200),
    time('Quicksort', 'quicksort/quicksort', 150),
    time('Binary tree (de)constructions', 'trees/trees', 10),
    time('Solving the n-queens problem', 'nqueens/nqueens', 500)
]

text = ""
for result in results:
    text += result.name + ':\n'
    text += '  Single-node: ' + ('%.3g' % result.singleTime) + '\n'
    text += '  OCaml:       ' + ('%.3g' % result.ocamlTime)  + '\n'
    text += '  Normalised   ' + ('%.3g' % (result.singleTime / result.ocamlTime)) + '\n'

# Write file
with open('benchmarks.txt', 'w') as f:
    f.write(text)
