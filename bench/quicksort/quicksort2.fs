quicksort@Q : (a -> a -> Bool) -> [a] -> [a]
  cmp []     = []
  cmp (h::t) = ((quicksort cmp l)) ++ [h] ++ ((quicksort cmp r))
    where
      l = filter (cmp h) t
      r = filter (not . cmp h) t

print_lists : [[Int]] -> IO ()
  []     = \x. ()
  (h::t) = print_list h ; print_lists t

print_list : [Int] -> IO ()
  xs = print_char '['
     ; print_body xs
     ; print_char ']'
     ; print_char '\n'
    where
      print_body : [Int] -> IO ()
        []     = \x. ()
        [x]    = print_int x
        (h::t) = print_int h ; print_char ',' ; print_body t

main@Main = (print_lists (map (quicksort (\x y. x < y))
                         (map reverse
                         (map (range 1)
                         ((range 175 200)))))
            ; terminate
            ) ()

-- Required functions

compose : (b -> c) -> (a -> b) -> a -> c
  g f x = g (f x)

append : [a] -> [a] -> [a]
  []     ys = ys
  (h::t) ys = h :: append t ys

seq : IO () -> IO a -> IO a
  a b x = b (a x)

type IO a = () -> a

filter : (a -> Bool) -> [a] -> [a]
  p []     = []
  p (h::t) = if p h
             then h :: filter p t
             else filter p t

not : Bool -> Bool
  True  = False
  False = True

range : Int -> Int -> [Int]
  start end = if start <= end
              then start :: range (start + 1) end
              else []

map : (a -> b) -> [a] -> [b]
  f []     = []
  f (h::t) = f h :: map f t

reverse : [a] -> [a]
  = reverse_acc []
    where
      reverse_acc : [a] -> [a] -> [a]
        acc xs = case xs of
          h::t -> reverse_acc (h :: acc) t
          _    -> acc
