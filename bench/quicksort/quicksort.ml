let rec filter p = function
  | [] -> []
  | h::t -> if p h
            then h :: filter p t
            else filter p t;;

let rec map f = function
  | [] -> []
  | h::t -> f h :: map f t;;

let reverse =
  let rec reverse' acc = function
    | [] -> acc
    | h::t -> reverse' (h::acc) t
  in
  reverse' [];;

let rec quicksort cmp = function
  | [] -> []
  | h::t -> let l = filter (fun x -> cmp x h) t in
            let r = filter (fun x -> not (cmp x h)) t in
            quicksort cmp l @ [h] @ quicksort cmp r;;

let print_list xs =
  let rec print_body = function
    | [] -> ()
    | [x] -> print_int x
    | h::t -> print_int h ; print_char ',' ; print_body t
  in
  print_char '[';
  print_body xs;
  print_string "]\n";;

let rec range start endd =
  if start <= endd
  then start :: range (start + 1) endd
  else [];;

let rec print_lists = function
  | [] -> ()
  | h::t -> print_list h ; print_lists t;;

print_lists (map (quicksort (<))
            (map reverse
            (map (range 1)
            (range 175 200))));;
