tak : Int -> Int -> Int -> Int
  x y z = if y < x
          then (tak ((tak ((x - 1)) y z))
                    ((tak ((y - 1)) z x)@D)
                    ((tak ((z - 1)) x y)))@B
          else z

main@Main = (print_int (tak 16 1 16); terminate) ()
seq a b x = b (a x)
