fib : Int -> Int
  n = if n <= 1
      then 1
      else fib (n-1) + fib (n-2)

main@Main = (print_int (fib 30) ; terminate) ()

type IO a = () -> a

seq : IO () -> IO a -> IO a
  a b x = b (a x)
