data Tree = Empty | Node Int Tree Tree

minN     : Int = 4
maxN     : Int = 18
stretchN : Int = maxN + 1

print@A : String -> Int -> Int -> IO ()
  f d c = f
        ; print_string " of depth "
        ; print_int d
        ; print_string "\t check: "
        ; print_int c
        ; print_char '\n'

print_depths@B : [(Int, Int, Int)] -> IO ()
  ((m,d,i)::t) = print (print_int m; print_string "\t trees") d i
               ; print_depths t
  []           = \x. ()

depth@C : Int -> [(Int, Int, Int)]
  d = if d <= maxN
      then let n    = (1 << (maxN - d + minN))
        in let s    = (sum d n 0)
        in let rest = (depth (d + 2))
        in ((2 * n, d, s) :: rest)
      else []

sum@D : Int -> Int -> Int -> Int
  d i t = if i == 0
          then t
          else let a   = (check (make i     d))
            in let b   = (check (make (0-i) d))
            in let ans = (a + b + t)
            in (sum d (i - 1) ans)

make@E : Int -> Int -> Tree
  i d = if d == 0
        then Node i Empty Empty
        else let i2 = (2 * i)
          in let d' = (d - 1)
          in (Node i ((make (i2 - 1) d')) ((make i2 d')))

check@F : Tree -> Int
  Empty        = 0
  (Node i l r) = i + ((check l)) - ((check r))

main@Main : IO ()
  = ( (let c = (check (make 0 stretchN)) in
       print (print_string "stretch tree") stretchN c)
    ; let long = (make 0 maxN) in
      print_depths ((depth minN))
    ; print (print_string "long lived tree") maxN (check long)
    ; terminate
    ) ()



type IO a = () -> a

seq : IO () -> IO a -> IO a
  a b x = b (a x)

print_string : String -> IO ()
  ""     = \x. ()
  (h::t) = print_char h ; print_string t

