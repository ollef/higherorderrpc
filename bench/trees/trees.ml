type 'a tree = Empty | Node of 'a tree * 'a * 'a tree

let rec make i d =
  if d = 0
  then Node(Empty, i, Empty)
  else let i2 = 2 * i
       and d' = d - 1 in
       Node(make (i2 - 1) d', i, make i2 d');;

let rec check = function
  | Empty -> 0
  | Node(l, i, r) -> i + check l - check r;;

let minN = 4;;
let maxN = 18;;
let stretchN = maxN + 1;;

let print = Printf.printf "%s of depth %i\t check: %i\n";;

let rec sum d i t =
  if i = 0
  then t
  else let a   = check (make i    d) in
       let b   = check (make (-i) d) in
       let ans = a + b + t in
       sum d (i - 1) ans;;

let rec depth d =
  if d <= maxN
  then let n    = 1 lsl (maxN - d + minN)
    in let s    = sum d n 0
    in let rest = depth (d + 2)
    in ((2 * n, d, s) :: rest)
  else [];;

let rec print_depths = function
  | (m,d,i)::t -> print (string_of_int m ^ "\t trees") d i;
                  print_depths t
  | []         -> ();;

let c = check (make 0 stretchN) in
print "stretch tree" stretchN c;;

let long_lived_tree = make 0 maxN;;

print_depths (depth minN);;

print "long lived tree" maxN (check long_lived_tree);;

