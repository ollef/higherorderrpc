import os
from time import perf_counter

class Result(object):
    def __init__(self, name, singleTime, multiTime, ocamlTime):
        self.name = name
        self.singleTime = singleTime
        self.multiTime = multiTime
        self.ocamlTime = ocamlTime

def time(name, file, runs):
    # Build the examples
    os.system('myrkotte --c-flag="-DPROFILE" -i ' + file + '2.fs -d ' + file + '_multi_files -o ' + file + '2.fs_run')

    # Run (and time!) them
    singleTime = 0.0
    multiTime  = 0.0
    ocamlTime  = 0.0
    for i in range(0, runs):
        multiTime  += getTime(file + '2.fs_run')

    # Clean up
    os.system('rm ' + file + '2.fs_run')
    os.system('rm -rf ' + file + '_single_files ' + file + '_multi_files')

    return Result(name, singleTime, multiTime, ocamlTime)

def getTime(s):
    start = perf_counter()
    os.system(s)
    end = perf_counter()
    diff = end - start
    return diff

results = [
    time('Naive generation of primes', 'naive-primes/primes', 1),
    time('The "tak" function', 'tak/tak', 1),
    time('Naive fibonacci generation', 'naive-fib/fib', 1),
    time('Quicksort', 'quicksort/quicksort', 1),
    time('Binary tree (de)constructions', 'trees/trees', 1),
    time('Solving the n-queens problem', 'nqueens/nqueens', 1)
]
