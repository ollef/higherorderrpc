let prime n =
  let rec prime' d =
    if d < n
    then n mod d <> 0 && prime' (d + 1)
    else true
  in prime' 2;;

let rec gen i n =
  if i < n
  then let primes = gen (i + 1) n
    in if prime i
       then i::primes
       else primes
  else [];;

let rec print_primes = function
  | []   -> ()
  | h::t -> print_int h; print_char '\n';;

print_primes (gen 2 10000);;
