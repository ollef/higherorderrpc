prime@Prime
  n = prime' 2
    where
      prime' d = if (d < (n/2))
                   then (((n % d /= 0)) && ((prime' (d + 1))))
                   else True

gen@Gen : Int -> Int -> [Int]
  i n = if i < n
        then let primes = gen (i + 1) n
          in if prime i
             then i::primes
             else primes
        else []

print_primes@PrintPrimes : [Int] -> IO ()
  []     = \x. ()
  (h::t) = print_int h
         ; print_char '\n'

main@A = (print_primes (gen 2 10000); terminate) ()

seq a b x = b (a x)

