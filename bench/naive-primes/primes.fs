prime : Int -> Bool
  n = prime' 2
    where
      prime' d = if d < n
                 then n % d /= 0 && prime' (d + 1)
                 else True

gen : Int -> Int -> [Int]
  i n = if i < n
        then let primes = gen (i + 1) n
          in if prime i
             then i::primes
             else primes
        else []

print_primes : [Int] -> IO ()
  []     = \x. ()
  (h::t) = print_int h
         ; print_char '\n'

main@A = (print_primes (gen 2 10000); terminate) ()

type IO a = () -> a

seq : IO () -> IO a -> IO a
  a b x = b (a x)

